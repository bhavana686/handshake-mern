var connection =  new require('./kafka/Connection');
//topics files
//var signin = require('./services/signin.js');
var signin = require('./services/signin');
var profile = require('./services/profile');
var passport = require('./services/passport');
var job = require('./services/jobs');
var event = require('./services/event');
var student = require('./services/student');
var message = require('./services/message');




const mongoose = require('mongoose');


const { mongoDB } = require('./config');
mongoose
  .connect(mongoDB, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true })
  .then(() => console.log('mongo connected'))
  .catch((err) => console.log(err));



  function handleTopicRequest(topic_name,fname){
      //var topic_name = 'root_topic';
      console.log("2222222");
      console.log(topic_name);
      var consumer = connection.getConsumer(topic_name);
      // consumer.setOffset(topic_name, 0, 1);
      var producer = connection.getProducer();
      console.log('server is running ');
      // consumer.close(true,(err,res)=>{
      //     console.log(err);
      //     console.log(res);
      // });
      consumer.on('message', function (message) {
          console.log("33333");
          console.log(message)
          console.log("11111111")
          console.log('message received for ' + topic_name +" ", fname);
          console.log("!!!!")
          console.log(JSON.stringify(message.value));
          console.log("!!!!");
         
          
         var data = JSON.parse(message.value);
          // data.replyTo = "response_topic"
          fname.handle_request(data.data, function(err,res){
              console.log('after handle'+res);
              
  

              var payloads = [
                  { topic: data.replyTo,
                      messages:JSON.stringify({
                          correlationId:data.correlationId,
                          data : res?res:err
                      }),
                      partition : 0
                  }
              ];
              console.log("Hello");
              console.log(payloads);
              producer.send(payloads, function(err, data){
                  console.log(data);
              });
              return;
          });
          
      });
  }
  // Add your TOPICs here
  //first argument is topic name
  //second argument is a function that will handle this topic request
  handleTopicRequest("signuplogin",signin)
  handleTopicRequest("passport",passport)
  handleTopicRequest("profile",profile)
  handleTopicRequest("jobs",job)
  handleTopicRequest("events",event)
  handleTopicRequest("students",student)
  handleTopicRequest("messages",message)


