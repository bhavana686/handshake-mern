const student = require('../models/student');
const company = require('../models/company');


function handle_request(msg, callback) {
    var res = {};
    console.log("12323232");
  console.log(msg);
    if (msg.logic === "getstudentprofile") {
        student.findOne({ studentid: msg.studentid }, (err, result) => {
            if (err) {
                res.status = 500;
                res.message = "Database Error";
                
            }
            if(result){
                res.status = 200;
                res.message = result;

            }
            else{
                res.status = 404;
                res.message = "not found";

            }

            callback(null, res);     
      });
    }
    else if(msg.logic === "getcompanyprofile"){
         company.findOne({ companyid: msg.companyid }, (err, result) => {
            if (err) {
                res.status = 500;
                res.message = "Database Error";
                
            }
            if(result){
                res.status = 200;
                res.message = result;

            }
            else{
                res.status = 404;
                res.message = "not found";

            }

            callback(null, res);     
      });
    
    }
    else if(msg.logic === "updatestudentbasic"){
            student.findOneAndUpdate({ studentid: msg.studentid }, 
              {
                name: msg.name,
                dateofbirth: msg.dateofbirth,
                city: msg.city,
                state: msg.state,
                country: msg.country,
              },
              {
                new: true,
              },
              (err, student) => {
                if (err) {
                  console.log(err);
                  res.status = 500;
                  res.message = err;
                  
                }
                res.status = 200;
                res.message = student;
                callback(null, res);     
              });
           
      }
      else if(msg.logic === "updatestudentcontact"){
        student.findOneAndUpdate({ studentid: msg.studentid }, 
          {
             mail: msg.mail,
             phonenumber: msg.phonenumber,
          },
          {
            new: true,
          },
          (err, student) => {
            if (err) {
              console.log(err);
              res.status = 500;
              res.message = err;
              
            }
            res.status = 200;
            res.message = student;
            callback(null, res);     
          });
       
  }
  else if(msg.logic === "updatestudentcareer"){
    student.findOneAndUpdate({ studentid: msg.studentid },
    {
        careerobjective: msg.careerobjective,
      },
      {
        new: true,
      },
      (err, student) => {
        if (err) {
          console.log(err);
          res.status = 500;
          res.message = err;
          
        }
        res.status = 200;
        res.message = student;
        callback(null, res);     
      });
   
}
else if(msg.logic === "updatestudentskillset"){
    student.findOneAndUpdate({ studentid: msg.studentid },
    {
        skillset: msg.skills,
      },
      {
        new: true,
      },
      (err, student) => {
        if (err) {
          console.log(err);
          res.status = 500;
          res.message = err;
          
        }
        res.status = 200;
        res.message = student.skillset;
        callback(null, res);     
      });
   
}
else if(msg.logic === "getstudentskillset"){
    student.findOne({ studentid: msg.studentid }, (err, result) => {
        if (err) {
            console.log(err);
            res.status = 500;
            res.message = err;
            
          }
          res.status = 200;
          res.message = (result?result.skillset?result.skillset:"":"");
          console.log(result);
          console.log(result?result.skillset?result.skillset:"":"");
          callback(null, res);     
        });
    }
else if(msg.logic === "addeducation"){
        const educations = {
            collegename: msg.collegename,
            educationlocation:msg.educationlocation,
            educationdegree: msg.educationdegree,
            educationmajor: msg.educationmajor,
            educationyearofpassing:msg.educationyearofpassing,
            educationcurrentcgpa: msg.educationcurrentcgpa,
            
            };
        student.updateOne({ studentid: msg.studentid }, { $push: { education: educations }, $sort: { educationyearofpassing: -1 } }, (err, student) => {
            if (err) {
                console.log(err);
                res.status = 500;
                res.message = err;
                
              }
              res.status = 200;
              res.message = student;
              callback(null, res);   
               
              });
            
    }
    else if(msg.logic === "addexperience"){
            const experiences = {
                companyname: msg.companyname,
                experiencetitle: msg.experiencetitle,
                experiencelocation: msg.experiencelocation,
                experiencestartdate: msg.experiencestartdate,
                experienceenddate: msg.experienceenddate,
                experienceworkdescription: msg.experienceworkdescription,
                    };
            student.updateOne({ studentid: msg.studentid }, { $push: { experience: experiences }, $sort: { experienceenddate: -1 } }, (err, student) => {
                    if (err) {
                        console.log(err);
                        res.status = 500;
                        res.message = err;
                        
                      }
                      res.status = 200;
                      res.message = student;
                      callback(null, res);   
                       
                      });
                    
                }
     else if(msg.logic === "deleteexperience"){
        student.update({ studentid: msg.studentid }, { $pull: { experience: { experiencedetailsid: msg.experiencedetailsid } } }, (err, student) => {
            if (err) {
                            console.log(err);
                            res.status = 500;
                            res.message = err;
                            
                          }
                          res.status = 200;
                          res.message = "success";
                          callback(null, res);   
                           
                          });
                        
                }   
              
   else if(msg.logic === "deleteeducation"){
        student.update({ studentid: msg.studentid }, { $pull: { education: { educationdetailsid: msg.educationdetailsid } } }, (err, student) => {
            if (err) {
                 console.log(err);
                res.status = 500;
                res.message = err;  
                                        
                }
                res.status = 200;
                res.message = "success";
                callback(null, res);   
                 
                });
              
      }   
             
      else if(msg.logic === "updateeducation"){
        student.findOneAndUpdate({ studentid: msg.studentid, 'education.educationdetailsid': msg.educationdetailsid },
        { $set: { 'education.$': msg } }, (err, student) => {
          if (err) {
            console.log(err);
            res.status = 500;
            res.message = err;  
          }
          res.status = 200;
          res.message = student;
          callback(null, res);   
        });
    }
    else if(msg.logic === "updateexperience"){
        student.findOneAndUpdate({ studentid: msg.studentid, 'experience.experiencedetailsid': msg.experiencedetailsid },
        { $set: { 'experience.$': msg } }, (err, student) => {
          if (err) {
            console.log(err);
            res.status = 500;
            res.message = err;  
          }
          res.status = 200;
          res.message = student;
          callback(null, res);   
        });
    }
    else if(msg.logic === 'updatecompanybasic'){
        console.log("in company basic")
    company.findOneAndUpdate({ companyid: msg.companyid }, 
       
          {
            companyname: msg.companyname,
            companylocation: msg.companylocation,
              companydescription: msg.companydescription,
          },
          {
            new: true,
          },
          (err, company) => {
            if (err) {
                
             console.log(msg.companyid)
              console.log(err);
              res.status = 500;
              res.message = err;
              
            }
            console.log(msg.companyid)
            console.log(company)
            res.status = 200;
            console.log(company)
            res.message = company;
            callback(null, res);     
          });
       
  }
  
  else if(msg.logic === "updatecompanycontact"){
    company.findOneAndUpdate({ companyid: msg.companyid }, 
      {
        companymail: msg.companymail,
      companyphonenumber: msg.companyphonenumber,
      },
      {
        new: true,
      },
      (err, company) => {
        if (err) {
          console.log(err);
          res.status = 500;
          res.message = err;
          
        }
        res.status = 200;
        res.message = company;
        callback(null, res);     
      });
   
  }
       

}








exports.handle_request = handle_request;