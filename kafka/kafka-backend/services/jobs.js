const student = require('../models/student');
const company = require('../models/company');
const event = require('../models/event');
const jobdetails = require('../models/jobdetails');


function handle_request(msg, callback) {
    var res = {};

    if (msg.logic === "studentjobsnotapplied") {
        let aggr = [{
            $lookup: {
                localField: 'companyid',
                from: 'companies',
                foreignField: 'companyid',
                as: 'company'
            }
        }, { $unwind: '$company' },
        {
            $project: {


                companyid: 1,
                jobtitle: 1,
                joblocation: 1,
                jobpostingdate: 1,
                jobapplicationdeadline: 1,
                jobdescription: 1,
                jobsalary: 1,
                jobid: 1,
                applications: {
                    $filter: {
                        input: "$applications",
                        as: "list",
                        cond: { $in: ["$$list.studentid", [msg.studentid]] } //<-- filter sub-array based on condition
                    }
                },
                jobcategory: 1,
                "companyname": "$company.companyname",


            }

        }
        ];
        jobdetails.aggregate(aggr, (err, result) => {
            if (err) {
                res.status = 500;
                res.message = "Database Error";

            }
            console.log('success');
            console.log(result);
            res.status = 200;
            res.message = result;
            callback(null, res);
        });
    }
    else if (msg.logic === "getstudentapplicationjobs") {
        jobdetails.find({ 'applications.studentid': msg.studentid }, (err, student) => {
            if (err) {
                res.status = 500;
                res.message = "Database Error";

            }
            console.log('success');
            console.log(student);
            res.status = 200;
            res.message = student;
            callback(null, res);
        });



    }
    else if (msg.logic === "getalljobs") {
        jobdetails.find({ companyid: msg.companyid }, (err, result) => {
            if (err) {
                res.status = 500;
                res.message = "Database Error";

            }
            console.log('success');
            console.log(result);
            res.status = 200;
            res.message = result;
            callback(null, res);
        });


    }
    else if (msg.logic === "studentsappliedtojob") {

        let aggr = [
            { $match: { jobid: msg.jobid } },

            {
                $lookup: {
                    localField: 'applications.studentid',
                    from: 'students',
                    foreignField: 'studentid',
                    as: 'students'
                }
            },
            {
                $project: {
                    'applications.applicationid': 1,
                    'applications.applicationstatus': 1,
                    'applications.applicationresume': 1,
                    'applications.name': 1,
                    'applications.mail': 1,
                    'applications.studentid': 1,

                }

            }
        ];
        jobdetails.aggregate(aggr, (err, result) => {
            if (err) {
                res.status = 500;
                res.message = "Database Error";
    
            }
            console.log('success');
            console.log(result);
            res.status = 200;
            res.message = result;
            callback(null, res);
        });
}
else if (msg.logic === "updateapplicationstatus") {
    jobdetails.findOneAndUpdate({  'applications.applicationid': msg.applicationid },
       { 'applications.$.applicationstatus': msg.applicationstatus } , (err, student) => {
        if (err) {
            res.status = 500;
            res.message = "Database Error";

        }
        console.log('success');
        console.log(student);
        res.status = 200;
        res.message = student;
        callback(null, res);
    });
}
else if (msg.logic === "addjob") {
    const newjobdetails = new jobdetails({
        companyid: msg.companyid,
        jobtitle: msg.jobtitle,
        jobpostingdate: msg.jobpostingdate,
        jobapplicationdeadline: msg.jobapplicationdeadline,
        joblocation: msg.joblocation,
        jobsalary: msg.jobsalary,
        jobdescription: msg.jobdescription,
        jobcategory: msg.jobcategory,
      });
      console.log(newjobdetails);
      newjobdetails.save((err, result) => {
        if (err) {
            res.status = 500;
            res.message = "Database Error";

        }
        console.log('success');
        console.log(result);
        res.status = 200;
        res.message = result;
        callback(null, res);
    });
}
    


    



}

exports.handle_request = handle_request;
