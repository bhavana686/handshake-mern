

const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const passport = require('passport');

const company = require('../models/company');
const student = require('../models/student');

function handle_request(msg, callback) {
  var res = {};

  student.findOne({ id: msg }, function (err, user) {
    if (err) {
      callback(null, false);
    }
    if (user) {
      callback(null, user);
    } else {
      callback(null, false);
    }
  });
};

       
  

  
exports.handle_request = handle_request;