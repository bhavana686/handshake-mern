const student = require('../models/student');
const company = require('../models/company');
const event = require('../models/event');
const jobdetails = require('../models/jobdetails');
const messages = require('../models/messages');


async function handle_request(msg,callback) {
  console.log("))))))))))))))))))))))))))))))))))))))")
  switch (msg.logic) {
      case 'addmessage':
          return addmessage(msg,callback)
      case 'allreceiver':
          return allreceiver(msg,callback)
      default:
          return { "status": 404, body: { message: 'Invalid Route in Kafka' } }
  }
}


 addmessage= async (msg,callback) => {
  var res = {};

  try {
    let msgObject = {
      userid: msg.user1id,
      
      messagecontent: msg.messagecontent
    }
    console.log(msgObject)
    console.log(msg.msgid)
    if (!msg.msgid) {
      console.log("bhavan")
  
  
  
      let msg1 = [];
      msg1.push(msgObject);
      const newmessages = new messages({
  
        user1id: msg.user1id,
        user1type: msg.user1type,
        user2id: msg.user2id,
        user2type: msg.user2type,
        message: msg1
      });
  
      newmessages.save((err, result) => {
        if (err) {
          res.status = 500;
          res.message = err;
          callback(null, res);     
  
        }
        else {
          res.status = 200;
          res.message = result;
          callback(null, res);     
  
  
        }
  
      });
    }
    else {
  
      messages.updateOne({ msgid: msg.msgid }, { $push: { message: msgObject } }, (err, student) => {
        if (err) {
          res.status = 500;
          res.message = err;
          callback(null, res);     
  
        }
        else {
          console.log(student)
  
  
          res.status = 200;
          res.message = student;
          callback(null, res);     
  
  
        }
      })
  
  
  
    } 
  } catch (ex) {
      console.log(ex)  

  }
}

  allreceiver= async (msg,callback) => {
    var res = {};
    console.log("))))))")
  try {
    let result = await messages.aggregate([
      {
        $match: {
          $or: [{ user1id: { $eq: msg.userid } }, { user2id: { $eq: msg.userid } }]
        }
      }]);
    let conList = [];
    console.log("uu")
    let userprofile;
    if (msg.usertype == "student") {
      userprofile = await student.findOne({ studentid: msg.userid }, (err, student) => {
  
        if (err) {
          res.status = 500;
          res.message = err;  
         
          
        }
        else {
  
          console.log("++")
          console.log(student)
          res.status = 200;
          res.message = student;
         
         
  
        }
      })
    }
    else {
      userprofile = await company.findOne({ companyid: msg.userid }, (err, student) => {
  
        if (err) {
          res.status = 500;
          res.message = err;
        
         
  
        }
        else {
  
          console.log("++")
          res.status = 200;
          res.message = student;
       
          
    
        }
      })
  
    }
    console.log("ll")
    console.log(userprofile)
    console.log("kk")
    if (msg.usertype == "student") {
      userprofile = {
        id: userprofile.studentid,
        name: userprofile.name,
        collegename: userprofile.collegename,
        profilepic: userprofile.profilepic,
      }
    }
    else {
      userprofile = {
  
        id: userprofile.companyid,
        name: userprofile.companynaame,
        location: userprofile.companylocation,
        profilepic: userprofile.companyprofilepic,
      }
    }
  
    for (conv of result) {
  
      console.log(conv.user1id)
  
      let tempuser = conv.user1id;
      let type = conv.user1type;
      if (tempuser == msg.userid) {
        tempuser = conv.user2id;
        type = conv.user2type;
      }
  
      if (type == 'student') {
        console.log("lllllllllllllll")
  
        console.log(tempuser)
        profile = await student.findOne({ studentid: tempuser }, (err, student) => {
          console.log(tempuser);
          if (err) {
              res.status = 500;
              res.message = err;
             
          }
          else {
  
              res.status = 200;
              res.message = student;
            
          }
        })
  
  
      }
      else {
        profile = await company.findOne({ companyid: tempuser }, (err,company) => {
          console.log(tempuser);
          if (err) {
              res.status = 500;
              res.message = err;
             
          }
          else {
  
              res.status = 200;
              res.message = company;
           
  
          }
        })
  
      }
  
      console.log(conv);
      console.log(profile);
      let image=profile?profile.companyprofilepic:"";
      console.log("mage")
      console.log(image)
      let conversation = {}
      if (type == 'student') {
        conversation = {
          ...conv,
          msgid: conv.msgid,
          id: tempuser,
          name: profile?profile.name:"",
          collegename: profile?profile.collegename:"",
          profilepic: profile?profile.profilepic:"",
        }
      }
      else {
        conversation = {
          ...conv,
          msgid: conv.msgid,
          id: tempuser,
          name: profile?profile.companyname:"",
          location: profile?profile.companylocation:"",
          companyprofilepic: profile?profile:"",
        
          
        }
      }
      console.log(conversation)
      conList.push(conversation);
    }
  
    let finalresult = {
      userprofile: userprofile,
      conList: conList,
      
    }
    res.status = 200;
    res.message = finalresult;
    callback(null, res); 
  
  }
catch (ex) {
  console.log(ex)
 
 
}
}
 

exports.handle_request = handle_request;