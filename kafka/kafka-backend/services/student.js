const student = require('../models/student');
const company = require('../models/company');
const event = require('../models/event');
const jobdetails = require('../models/jobdetails');


function handle_request(msg, callback) {
    var res = {};
  
    if (msg.logic === "getallotherstudents") {
        
        student.find({ studentid: { $ne: msg.studentid } }, (err, student) => {
            if (err) {
                res.status = 500;
                res.message = "Database Error";
                
            }
            console.log(student);
       
            res.status = 200;
            res.message = student;
            callback(null, res);     
          
        });
    }
      
        if (msg.logic === "getallstudents") {
            student.find({}, (err, student) => {
                if (err) {
                    res.status = 500;
                    res.message = "Database Error";
                    
                }
                console.log(student);
           
                res.status = 200;
                res.message = student;
                callback(null, res);     
              
            });
        }
        
        if (msg.logic === "getmajor") {
            student.findOne({ studentid: msg.studentid}, (err, student) => {
                if (err) {
                    res.status = 500;
                    res.message = "Database Error";
                    
                }
                console.log(student);
           
                res.status = 200;
                res.message = (student.education?student.education[0]?student.education[0].educationmajor:"":"");
                callback(null, res)
                 
            });
          
           
           
        }
        if (msg.logic === "getresume") {
            jobdetails.find({ 'applications.applicationid': msg.applicationid }, (err, result) => {
                if (err) {
                     console.log(err);
                     res.status = 500;
                     res.message = "Database Error";
                }
                console.log(result);
                let ans= result.applications?result.applications[0].applicationresume?result.applications[0].applicationresume:"":"";
                console.log("555555555555");
                console.log(ans);
                res.status = 200;
                res.message = (result[0].applications?result[0].applications[0].applicationresume:"");
                callback(null, res)
                 




              });
        
           
           
        }
          
      
}
        
exports.handle_request = handle_request;
