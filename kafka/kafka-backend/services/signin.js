const student = require('../models/student');
const company = require('../models/company');
var bcrypt = require('bcryptjs');
const saltRounds = 10;

function handle_request( msg, callback) {
    console.log(msg)
  var res = {};

  if (msg.logic == "studentsignup") {
    
    bcrypt.hash(msg.password, saltRounds, (err, hash) => 
    {
      const newstudent = new student({
        name: msg.name,
        mail: msg.mail,
        password: hash,
        collegename: msg.collegename,
    
      });
    student.findOne({ mail: msg.mail}, (err, user) => {
      if (err) {
        res.status = 500;
        res.message = "Error in Data";
        callback(null, res);
      }
      if (user) {
        res.status = 500;
        console.log("already exist")

        res.message = "STUDENT_EXISTS";
        callback(null, res);
      }
      else {
        newstudent.save((err, data) => {
          if (err) {
            console.log("not added")
            res.status = 500;
            res.message = "Error in Data";
            callback(null, res);
          }
          else {
            console.log("added")
            res.status = 200;
            res.message = "STUDENT_ADDED";
            callback(null, res);
          }
        });
      }
    });
    })
    
  
  }
  else if (msg.logic == "companysignup") {

    const newcompany = new company({
      companyname: msg.companyname,
      companymail: msg.companymail,
      password:msg.password,
      companylocation: msg.companylocation,
     
  
    });

    company.findOne({ companymail: msg.companymail,password:msg.password }, (err, user) => {
      if (err) {
        res.status = 500;
        res.message = "Error in Data";
        callback(null, res);
      }
      
      if (user) {
        res.status = 500;
        res.message = "COMPANY_EXISTS";
        callback(null, res);
      }
      else {
        newcompany.save((err, data) => {
          if (err) {
            res.status = 500;
            res.message = "Error in Data";
            callback(null, res);
          }
          else {
            res.status = 200;
            res.message = "COMPANY_ADDED";
            callback(null, res);
          }
        });
      }
    });
  }
  else if (msg.logic == "studentlogin")
   {
    student.findOne({ mail: msg.mail }, (error, student) => {
        console.log('student login');
    
        if (error) {
          res.status = 500;
          res.message = "Error";
          callback(null, res);
        
        }
        if (!student) {
          res.status = 402;
          res.message = "STUDENT_NOTEXISTS";
          callback(null, res);
        
        }
        if (student) {
          bcrypt.compare(msg.password, student.password, (err, result1) => {
            if (err) {
              res.status = 500;
              res.message = "Error";
              callback(null, res);
            
            }
            if(result1)
            {
              const payload = { studentid: student.studentid, mail: student.mail,name:student.name };
              res.status = 200;
              res.message = payload; 
              callback(null, res);

            }
            else{
              console.log('3');
               res.status = 401;
             res.message = "INCORRECT_PASSWORD";
              callback(null, res);

            }
      
          
          })
         }
         else {
          console.log('3');
          res.status = 401;
          res.message = "ERROR";
          callback(null, res);
        }
      });
    
    }
    else
    {
      company.findOne({ companymail: msg.companymail, password: msg.password }, (error, company) => {
        console.log('company login');
      
        if (error) {
          res.status = 500;
          res.message = "Error";
          callback(null, res);
        
        }
        if (!company) {
          res.status = 402;
          res.message = "COMPANY_NOTEXISTS";
          callback(null, res);
        
        }
        if (company) {
          console.log('2');
          const payload = { companyid: company.companyid, companymail: company.companymail,companyname:company.companyname };
          res.status = 200;
          res.message = payload; 
          callback(null, res);
         }
         else {
          console.log('3');
          res.status = 401;
          res.message = "INCORRECT_PASSWORD";
          callback(null, res);
        }
      });
    

    }
    
  
}

  
      

exports.handle_request = handle_request;



 