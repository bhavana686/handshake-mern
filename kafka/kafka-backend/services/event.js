const student = require('../models/student');
const company = require('../models/company');
const event = require('../models/event');
const jobdetails = require('../models/jobdetails');

function handle_request(msg, callback) {
    var res = {};
  
    if (msg.logic === "eventsnotregistered") {
        event.find({ 'registrations.studentid': { $ne: msg.studentid } }, (err, event) => {
                if (err) {
                    res.status = 500;
                    res.message = "Database Error";
                }
                console.log(event);
           
                res.status = 200;
                res.message = event;
                callback(null, res);     
              });
            
              
    }
    else if (msg.logic === "getallevents") {
            event.find({}, (err, event) => {
                if (err) {
                    res.status = 500;
                    res.message = "Database Error";
                }
                console.log(event);
           
                res.status = 200;
                res.message = event;
                callback(null, res);     
              });
            
              
    }
    else if (msg.logic === "eventsregistered") {
        event.find({ 'registrations.studentid': msg.studentid }, (err, event) => {
            if (err) {
                res.status = 500;
                res.message = "Database Error";
            }
            console.log(event);
           
            res.status = 200;
            res.message = event;
            callback(null, res);     
          });

          
}
else if (msg.logic === "registerforevent") {
        console.log(' student register for event');
  const registration = {
    eventid: msg.eventid,
    studentid: msg.studentid,
    eventstatus: msg.eventstatus,
    name:msg.name,
    mail:msg.mail,
  };
  event.update({ eventid: msg.eventid }, { $push: { registrations: registration } }, (err, event) => {
    if (err) {
        res.status = 500;
        res.message = "Database Error";
    }
    console.log(event);
   
    res.status = 200;
    res.message = event;
    callback(null, res);     
  });

      
}
else if (msg.logic === "getcompanyevents") {
  
    event.find({ companyid: msg.companyid }, (err, result) => {
        if (err) {
            res.status = 500;
            res.message = "Database Error";
        }
       
   
        res.status = 200;
        res.message = result;
        callback(null, res);     
      });
    
  
}    
else if (msg.logic === "addevent") {
    
  const newevent = new event({
    companyid: msg.companyid,
    eventname: msg.eventname,
    eventdescription: msg.eventdescription,
    eventtime: msg.eventtime,
    eventdate: msg.eventdate,
    eventlocation: msg.eventlocation,
    eventeligibility: msg.eventeligibility,


  });
  newevent.save((err, result) => {
    if (err) {
        res.status = 500;
        res.message = "Database Error";
    }
   

    res.status = 200;
    res.message = result;
    callback(null, res);     
  });

  
  
}  
else if (msg.logic === "geteventdetails") {
    event.find({ eventid: msg.eventid }, (err, result) => {
        if (err) {
            res.status = 500;
            res.message = "Database Error";
        }
       
   
        res.status = 200;
        res.message = result;
        callback(null, res);     
      });
    
    
  
}   
else if (msg.logic === "studentsappliedtoevent") {
    let aggr = [ 
        { $match: { eventid: msg.eventid } },
        
        {
        $lookup: {
            localField: 'registrations.studentid',
            from: 'students',
            foreignField: 'studentid',
            as: 'students'
        }
      },
      {
        $project: {
            'registrations.name':1,
            'registrations.mail':1,
            'registrations.studentid':1,
             name:1,
             mail:1,
           
        }
      
      }
      ];
      event.aggregate(aggr, (err, result) => {
        if (err) {
            res.status = 500;
            res.message = "Database Error";
        }
       
   
        res.status = 200;
        res.message = result;
        callback(null, res);     
      });
      
    
  
}  
else if (msg.logic === "studenteventdetails") {
    let aggr = [ 
        { $match: { eventid: msg.eventid  }},
         {
         $lookup: {
             from: 'students',
             localField: 'registrations.studentid',
             foreignField: 'studentid',
             as: 'students'
         }
       },
       {
         $project: {
             "eventname":1,
             "eventdate":1,
            
           "eventdescription":1,
           "eventtime":1,
           
           "eventlocation":1,
           "eventeligibility":1,
           students: {$filter: {
             input: "$students",
             as: "list",
             cond: {$in: ["$$list.studentid", [msg.studentid]]} //<-- filter sub-array based on condition
         }} ,
             
             //"students":1
           
            
         }
       
       }
       ];
       event.aggregate(aggr, (err, result) => {
        if (err) {
            res.status = 500;
            res.message = "Database Error";
        }
       
   
        res.status = 200;
        res.message = result;
        callback(null, res);     
      });
}                  
      
}
        
exports.handle_request = handle_request;
