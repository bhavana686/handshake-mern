/* eslint-disable no-console */
const express = require('express');
const jwt = require('jsonwebtoken');
const kafka = require('../../kafka/client');

const router = express.Router();
const { checkAuth } = require("../helpers/passport");


const JwtStrategy = require('passport-jwt').Strategy;
const company = require('../models/company');
const jobdetails = require('../models/jobdetails');
const event = require('../models/event');
const student = require('../models/student');
const passport = require('passport');
const { secret } = require('../helpers/setting');

const { auth } = require("../helpers/passport");
auth();
console.log("In Company Route");




router.post('/companysignup', (req, res) => {
   console.log(req.body)
  kafka.make_request('signuplogin', req.body, (err, results) => {
    if (err) {
      res.status(500).end('System Error');
    } else {
      res.status(results.status).send(results.message);
    }
  });
});

router.post('/companylogin', (req, res) => {
  kafka.make_request('signuplogin', req.body, (err, results) => {
    if (err) {
      res.status(500).end("System Error");
    }
    else if (results.status === 200) {
      let payload = results.message;
      var token = jwt.sign(payload, secret, {
        expiresIn: 1008000
      });
      res.json({ success: true, token: 'JWT ' + token });
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});

router.get('/getcompanyprofile/:companyid', checkAuth ,  (req, res) => { 
  req.body.logic = "getcompanyprofile";
  req.body.companyid = req.params.companyid;
  kafka.make_request('profile', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});

router.post('/updatecompanycontactdetails/:companyid', checkAuth , (req, res) => { 
  req.body.logic = "updatecompanycontact";
  req.body.companyid = req.params.companyid;
  kafka.make_request('profile', req.body, function (err, results) {
    if (err) {
      res.status(500).send("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});
router.post('/updatecompanybasicdetails/:companyid', checkAuth , (req, res) => { 
  console.log("in company update basic")
  req.body.logic = "updatecompanybasic";
  req.body.companyid = req.params.companyid;
  console.log(req.body)
  kafka.make_request('profile', req.body, function (err, results) {
    if (err) {
      res.status(500).send("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});


router.get('/getalljobs/:companyid',  checkAuth ,(req, res) => {
  req.body.logic = "getalljobs";
  req.body.companyid = req.params.companyid;
  kafka.make_request('jobs', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});
router.post('/studentsappliedtojob', checkAuth , (req, res) => {
  req.body.logic = "studentsappliedtojob";
  kafka.make_request('jobs', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});
  
router.post('/updateapplicationstatus',  checkAuth ,(req, res) => {
  req.body.logic = "updateapplicationstatus";
  kafka.make_request('jobs', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});

router.post('/addjob', checkAuth ,(req, res) => {
  req.body.logic = "addjob";
  kafka.make_request('jobs', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
})
router.post('/addevent', checkAuth , (req, res) => {
  req.body.logic = "addevent";
  kafka.make_request('events', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
})

module.exports = router;