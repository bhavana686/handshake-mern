/* eslint-disable no-console */
const express = require('express');
const jwt = require('jsonwebtoken');
const kafka = require('../../kafka/client');


const { checkAuth } = require("../helpers/passport");


const JwtStrategy = require('passport-jwt').Strategy;



const router = express.Router();

const passport = require('passport');


const event = require('../models/event');
const company = require('../models/company');
const jobdetails = require('../models/jobdetails');

const student = require('../models/student');
const { secret } = require('../helpers/setting');

const { auth } = require("../helpers/passport");




router.post('/getallevents',checkAuth, (req, res) => {
  req.body.logic = "getallevents";
  kafka.make_request('events', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});
  
router.post('/eventsregistered', checkAuth,(req, res) => {
  req.body.logic = "eventsregistered";
  kafka.make_request('events', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});
  
router.post('/eventsnotregistered',checkAuth, (req, res) => {
  req.body.logic = "eventsnotregistered";
  kafka.make_request('events', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});

router.post('/registerforevent', checkAuth, (req, res) => {
  req.body.logic = "registerforevent";
  kafka.make_request('events', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});
  
router.get('/getallevents/:companyid',checkAuth, (req, res) => {
  req.body.logic = "getcompanyevents";
  req.body.companyid = req.params.companyid;
  kafka.make_request('events', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});
  

router.get('/geteventdetails/:eventid', checkAuth,(req, res) => {
  req.body.logic = "geteventdetails";
  req.body.eventid = req.params.eventid;
  kafka.make_request('events', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});
router.post('/studentsappliedtoevent',checkAuth, (req, res) => {
  req.body.logic = "studentsappliedtoevent";
  kafka.make_request('events', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});

module.exports = router;