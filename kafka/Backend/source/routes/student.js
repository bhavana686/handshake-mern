/* eslint-disable no-shadow */
/* eslint-disable max-len */
/* eslint-disable no-console */
/* eslint-disable consistent-return */
const express = require('express');

const JwtStrategy = require('passport-jwt').Strategy;
const jwt = require('jsonwebtoken');
const kafka = require('../../kafka/client');

const router = express.Router();
const student = require('../models/student');
const event = require('../models/event');
const jobdetails = require('../models/jobdetails');
const company= require('../models/company');
const passport = require('passport');
const { checkAuth } = require("../helpers/passport");
const { secret } = require('../helpers/setting');

const { auth } = require("../helpers/passport");
auth();



router.post('/studentsignup', (req, res) => {
  console.log(req.body)
  kafka.make_request('signuplogin', req.body, (err, results) => {
    if (err) {
      res.status(500).end('System Error');
    } else {
      res.status(results.status).send(results.message);
    }
  });
});


router.post('/studentlogin', (req, res) => {
  kafka.make_request('signuplogin', req.body, (err, results) => {
    if (err) {
      res.status(500).end("System Error");
    }
    else if (results.status === 200) {
      let payload = results.message;
      var token = jwt.sign(payload, secret, {
        expiresIn: 1008000
      });
      res.json({ success: true, token: 'JWT ' + token });
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});




router.get('/getstudentprofile/:studentid',  checkAuth ,(req, res) => { 
  req.body.logic = "getstudentprofile";
  req.body.studentid = req.params.studentid;
  kafka.make_request('profile', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});

router.post('/updatestudentbasicdetails/:studentid', checkAuth ,(req, res) => { 
  req.body.logic = "updatestudentbasic";
  req.body.studentid = req.params.studentid;
  kafka.make_request('profile', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});

  

router.post('/updatestudentcontactinfo/:studentid',  checkAuth ,(req, res) => { 
  req.body.logic = "updatestudentcontact";
  req.body.studentid = req.params.studentid;
  kafka.make_request('profile', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});



router.post('/updatestudentcareerobjective/:studentid', checkAuth , (req, res) => { 
  req.body.logic = "updatestudentcareer";
  req.body.studentid = req.params.studentid;
  kafka.make_request('profile', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});

router.get('/getskillset/:studentid', checkAuth , (req, res) => { 
  req.body.logic = "getstudentskillset";
  req.body.studentid = req.params.studentid;
  console.log('inside skill');
  kafka.make_request('profile', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});
router.post('/updatestudentskillset/:studentid',  checkAuth ,(req, res) => { 
  req.body.logic = "updatestudentskillset";
  req.body.studentid = req.params.studentid;
  kafka.make_request('profile', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});

router.post('/addeducation/:studentid', checkAuth , (req, res) => { 
  req.body.logic = "addeducation";
  req.body.studentid = req.params.studentid;
  kafka.make_request('profile', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});
 
  
router.post('/addexperience/:studentid',  checkAuth , (req, res) => { 
  req.body.logic = "addexperience";
  req.body.studentid = req.params.studentid;
  kafka.make_request('profile', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});
router.post('/deleteeducation/:studentid/', checkAuth ,(req, res) => { 
  req.body.logic = "deleteeducation";
  req.body.studentid = req.params.studentid;
  console.log('delete education student');
  kafka.make_request('profile', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});

router.post('/deleteexperience/:studentid/', checkAuth , (req, res) => { 
  req.body.logic = "deleteexperience";
  req.body.studentid = req.params.studentid;
  console.log('delete experience student');
  kafka.make_request('profile', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});

router.post('/updateeducation/:studentid',  checkAuth ,(req, res) => { 
  req.body.logic = "updateeducation";
  req.body.studentid = req.params.studentid;
  kafka.make_request('profile', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});


router.post('/updateexperience/:studentid', checkAuth , (req, res) => { 
  req.body.logic = "updateexperience";
  req.body.studentid = req.params.studentid;
  kafka.make_request('profile', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});

router.post('/jobsnotapplied',  checkAuth ,(req, res) => {
  req.body.logic = "studentjobsnotapplied";
  kafka.make_request('jobs', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});
router.post('/getallotherstudents',  checkAuth ,(req, res) => {
  req.body.logic = "getallotherstudents";
  kafka.make_request('students', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  }); 
});

router.post('/getstudentapplicationjobs',  checkAuth ,(req, res) => {
  req.body.logic = "getstudentapplicationjobs";
  kafka.make_request('jobs', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
});


router.get('/getallstudents', checkAuth ,(req, res) => {
  req.body.logic = "getallstudents";
  kafka.make_request('students', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
})
  
router.post('/studenteventdetails', checkAuth ,(req, res) => {
  req.body.logic = "studenteventdetails";
  kafka.make_request('events', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
})
  

router.post('/getmajor', checkAuth ,(req, res) => {
  req.body.logic = "getmajor";
  kafka.make_request('students', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
})

router.post('/getresume', checkAuth ,(req, res) => {
  req.body.logic = "getresume";
  kafka.make_request('students', req.body, function (err, results) {
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      res.status(results.status).send(results.message);
    }
  });
})
module.exports = router;