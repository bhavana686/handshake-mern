
const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');
const uuid = require('uuid');


const { Schema } = mongoose;


const event = new Schema({
  eventid: {
    type: String,
    default: uuid.v1,
  },
  eventname: {
    type: String,
    required: true,
  },
  eventdescription: {
    type: String,
    required: true,
  },
  eventtime: {
    type: String,
    required: true,
  },
  eventdate: {
    type: Date,
    required: true,
  },
  eventlocation: {
    type: String,
    required: true,
  },
  eventeligibility: {
    type: String,
    required: true,
  },
  companyid: {
    type: String,
    required: true,
  },
  registrations: [
    {
      registrationid: { type: String, default: uuid.v1 },
      studentid: String,
      eventid: String,
      jobid: String,
      companyid: String,
      eventstatus: String,
      name:String,
      mail:String,

    },
  ],

});


event.plugin(uniqueValidator);

module.exports = mongoose.model('event', event);
