/* eslint-disable no-unused-vars */

const express = require('express');

const passportJWT = require('passport-jwt');

const rooturl = 'http://35.193.158.185:3000';
const port = process.env.PORT || 5000;
const app = express();
const bodyParser = require('body-parser');
const session = require('express-session');
const cors = require('cors');
const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const mongoose = require('mongoose');

const JwtStrategy = require('passport-jwt').Strategy;
const kafka = require('../Backend/kafka/client');


const multer = require('multer');
var jobSchema = require('../Backend/source/models/jobdetails');
var studentSchema = require('../Backend/source/models/student');
var companySchema = require('../Backend/source/models/company');
var messages = require('../Backend/source/models/messages');

const jwt = require('jsonwebtoken');

app.set('view engine', 'ejs');


app.use(express.static('public'));
const db = require('./source/helpers/setting').mongoURI;

mongoose
  .connect(db, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true })
  .then(() => console.log('mongo connected'))
  .catch((err) => console.log(err));

console.log('********');




const storage = multer.diskStorage({

  destination: (req, file, cb) => {
    if (file.mimetype === 'application/pdf') {
      cb(null, './public/resume');
    } else {
      cb(null, './public/profilepic');
    }
  },
  filename: (req, file, cb) => {
    if (file.mimetype === 'application/pdf') {
      console.log(req.body.jobid + req.body.studentid);
      cb(null, req.body.jobid + req.body.studentid + path.extname(file.originalname));
    } else {
      cb(null, req.body.id + path.extname(file.originalname));
    }
  },
});
const upload = multer({
  storage,
});

const AWS = require('aws-sdk');
const pool = require('./source/dbconnect/pool');

const s3 = new AWS.S3({
  accessKeyId: 'AKIAI72XJ4B436BWHDZA',
  secretAccessKey: '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',
});


// use cors to allow cross origin resource sharing
//  app.use(cors({ origin: rooturl, credentials: true }));
app.use(cors({ origin: true, credentials: true }));
const connection = mongoose.createConnection(db, {
  useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true,
});


// use express session to maintain session data
app.use(session({
  secret: 'cmpe273_kafka_passport_mongo',
  resave: false, // Forces the session to be saved back to the session store, even if the session was never modified during the request
  saveUninitialized: false, // Force to save uninitialized session to db. A session is uninitialized when it is new but not modified.
  duration: 60 * 60 * 1000, // Overall duration of Session : 30 minutes : 1800 seconds
  activeDuration: 5 * 60 * 1000,
}));


// Allow Access Control
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', rooturl);
  res.setHeader('Access-Control-Allow-Credentials', 'true');
  res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
  res.setHeader('Cache-Control', 'no-cache');
  next();
});

const student = require('./source/routes/student');
const company = require('./source/routes/company');
const event = require('./source/routes/event');


app.use(express.static('public'));
const studentPath = '/student';
const companyPath = '/company';
const eventPath = '/event';

app.use(express.json());
app.use(bodyParser.json());
app.use(cors({ origin: rooturl, credentials: true }));
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(studentPath, student);
app.use(companyPath, company);
app.use(eventPath, event);
app.use('/uploads', express.static(path.join(__dirname, '/uploads/')));




app.post('/updateresume', upload.single('resume'), async (request, response) => {
  try {
    if (request.file) {
      const fileContent = fs.readFileSync(`./public/resume/${request.body.jobid}${request.body.studentid}${path.extname(request.file.originalname)}`);
      // console.log(fileContent)
      const params = {
        Bucket: 'handshakeresume-273',
        Key: `${request.body.jobid}${request.body.studentid}${path.extname(request.file.originalname)}`,
        Body: fileContent,
        ContentType: request.file.mimetype,
      };
      console.log('--------');
      console.log(params);

      // eslint-disable-next-line consistent-return
      s3.upload(params, (err, data) => {
        if (err) {
          return response.status(500).json({ error: err.message });
        }
        // console.log(data);
        const application = {
          collegename: request.body.collegename,
          jobid: request.body.jobid,
          studentid: request.body.studentid,
          applicationresume: data.Location,
          name: request.body.name,
          mail: request.body.mail,
          applicationstatus:request.body.applicationstatus

        };
        jobSchema.findOneAndUpdate({ jobid: request.body.jobid }, { $push: { applications: application } }, (err, result) => {
          console.log('--------*******');
          // console.log(result);
          if (err) {
            console.log(err);
            return response.status(500).json({ error: err.message });
          }

          return response.status(200).json(result);
        })
      });
    }
  } catch (ex) {
    const message = ex.message ? ex.message : 'Error while uploading resume';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});

app.post('/uploadstudentpic', upload.single('profilepic'), async (request, response) => {
  try {
    if (request.file) {
      const fileContent = fs.readFileSync(`./public/profilepic/${request.body.id}${path.extname(request.file.originalname)}`);
      // console.log(fileContent)
      const params = {
        Bucket: 'handshakeresume-273',
        Key: `${request.body.id}${path.extname(request.file.originalname)}`,
        Body: fileContent,
        ContentType: request.file.mimetype,
      };
      console.log('--------');
      console.log(params);

      // eslint-disable-next-line consistent-return
      s3.upload(params, (err, data) => {
        if (err) {
          return response.status(500).json({ error: err.message });
        }
        studentSchema.findOneAndUpdate({ studentid: request.body.id },
          {
            profilepic: data.Location

          },
          {
            new: true,
          }, (err, result) => {
            console.log('--------*******');
            // console.log(result);
            if (err) {
              console.log(err);
              return response.status(500).json({ error: err.message });
            }
            console.log('success');
            return response.status(200).json(data.Location);
          })
      });
    }
  } catch (ex) {
    const message = ex.message ? ex.message : 'Error while uploading resume';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});




app.post('/uploadcompanypic', upload.single('profilepic'), async (request, response) => {
  try {
    if (request.file) {
      console.log('company profile');
      const fileContent = fs.readFileSync(`./public/profilepic/${request.body.id}${path.extname(request.file.originalname)}`);
      // console.log(fileContent)
      const params = {
        Bucket: 'handshakeresume-273',
        Key: `${request.body.id}${path.extname(request.file.originalname)}`,
        Body: fileContent,
        ContentType: request.file.mimetype,
      };
      console.log('--------');
      console.log(params);

      // eslint-disable-next-line consistent-return
      s3.upload(params, (err, data) => {
        if (err) {
          return response.status(500).json({ error: err.message });
        }
        companySchema.findOneAndUpdate({ companyid: request.body.id },
          {
            profilepic: data.Location

          },
          {
            new: true,
          }, (err, result) => {
            console.log('--------*******');
            // console.log(result);
            if (err) {
              console.log(err);
              return response.status(500).json({ error: err.message });
            }
            console.log('success');
            return response.status(200).json(data.Location);
          })
      });
    }
  } catch (ex) {
    const message = ex.message ? ex.message : 'Error while uploading resume';
    const code = ex.statusCode ? ex.statusCode : 500;
    return response.status(code).json({ message });
  }
});



app.post('/addmessage', async (req, res) => {
  console.log("))))))))))))))))))))))))))))))))))))))")
    req.body.logic = "addmessage";

     kafka.make_request('messages', req.body, function (err, results) {
      if (err) {
        res.status(500).end("System Error");
      }
      else {
        res.status(results.status).send(results.message);
      }
    });
});
app.post('/allreceivers', async (req, res) => {
  req.body.logic = "allreceiver";
  kafka.make_request('messages', req.body, function (err, results) {
    console.log("))))))))))))))))))))))))))))))))))))))")
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      console.log(results)
      res.status(results.status).send(results.message);
    }
  });
})



app.post('/allreceivers', async (req, res) => {
  req.body.logic = "allreceiver";
  kafka.make_request('messages', req.body, function (err, results) {
    console.log("))))))))))))))))))))))))))))))))))))))")
    if (err) {
      res.status(500).end("System Error");
    }
    else {
      console.log(results)
      res.status(results.status).send(results.message);
    }
  });
})





app.listen(port);
console.log(`Server Listening port ${port}`);
module.exports = app;
