import { GET_COMPANYJOBLIST,GET_RESUME, ADD_JOB,APPLY_FORJOB,
     GET_ALLJOBLIST ,GET_STUDENTSAPPLIEDTOJOB, UPDATE_APPLICATIONSTATUS,} from "./types";

import {rooturl} from '../webConfig';

import axios from "axios";

export const getCompanyJoblist = (data) => dispatch => {
    axios.defaults.withCredentials = true;
    console.log(data.companyid);
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.get(`${rooturl}/company/getalljobs/${sessionStorage.getItem("companyid")}`)
        .then(response => dispatch({
            type: GET_COMPANYJOBLIST,
            payload: response.data
        }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: GET_COMPANYJOBLIST,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const addJobPost = (data) => dispatch => {
    console.log(data);
    axios.defaults.withCredentials = true;
    console.log(data.companyid);
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/company/addjob`,data)
        .then(response => dispatch({
            type: ADD_JOB,
            payload: response.data
        }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: ADD_JOB,
                    payload: error.response.data
                });
            }
            return;
        });
}

export const getAlljobs = () => dispatch => {
    const data = {
        studentid: sessionStorage.getItem('studentid')
    }
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/student/jobsnotapplied`, data)
        .then(response => dispatch({
            type: GET_ALLJOBLIST,
            payload: response.data
        }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: GET_ALLJOBLIST,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const getStudentsAppliedToJob = (data) => dispatch => {
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
        axios.post(`${rooturl}/company/studentsappliedtojob`,data)
        .then(response => dispatch({
            type: GET_STUDENTSAPPLIEDTOJOB,
            payload: response.data
        }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: GET_STUDENTSAPPLIEDTOJOB,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const getResume = (data) => dispatch => {
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/student/getresume`,data)
        
        .then(response => dispatch({
            type: GET_RESUME,
            payload: response.data
        }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: GET_RESUME,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const updateApplicationStatus = (data) => dispatch => {
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/company/updateapplicationstatus`, data)
        
        .then(response => dispatch({
            type: UPDATE_APPLICATIONSTATUS,
            payload: response.data
        }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type:  UPDATE_APPLICATIONSTATUS,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const applyForJob = (formData, config) => dispatch => {
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/updateresume`, formData, config)
    .then(response => dispatch({
        type: APPLY_FORJOB,
        payload: response.data
    }))
    .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type:  APPLY_FORJOB,
                    payload: error.response.data
                });
            }
            return;
        });
}
