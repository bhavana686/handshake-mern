import { GET_RECEIVERS ,ADD_MESSAGE} from "./types";
import {rooturl} from '../webConfig';
import axios from "axios";

export const getFriends = (data) => dispatch => {
    axios.defaults.withCredentials = true;
    console.log(data.companyid);
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
   // axios.get(`${rooturl}/company/getalljobs/${sessionStorage.getItem("companyid")}`)
    axios.post(`${rooturl}/allreceivers`, data)
        .then(response => dispatch({
            type: GET_RECEIVERS,
            payload: response.data
        }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: GET_RECEIVERS,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const addmessage = (data) => dispatch => {
    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/addmessage`, data)
    .then(response => dispatch({
        type: ADD_MESSAGE,
        payload: response.data
    }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: ADD_MESSAGE,
                    payload: error.response.data
                });
            }
            return;
        });
}