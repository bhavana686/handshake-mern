import { GET_ALLSTUDENTS ,GET_ALLOTHERSTUDENTS} from "./types";


import {rooturl} from '../webConfig';

import axios from "axios";

export const getAllotherstudents = (data) => dispatch => {
    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/student/getallotherstudents`,data)
        .then(response => dispatch({
            type: GET_ALLOTHERSTUDENTS,
            payload: response.data
        }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: GET_ALLOTHERSTUDENTS,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const getAllstudents = (data) => dispatch => {
    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.get(`${rooturl}/student/getallstudents`)
        .then(response => dispatch({
            type: GET_ALLSTUDENTS,
            payload: response.data
        }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: GET_ALLSTUDENTS,
                    payload: error.response.data
                });
            }
            return;
        });
}