
import { GET_COMPANYPROFILE, GET_STUDENTPROFILE } from "./types";
import { UPDATE_COMPANYPROFILE, UPDATE_STUDENTPROFILE } from "./types";


import {rooturl} from '../webConfig';

import axios from "axios";

export const getStudentprofile = (data) => dispatch => {
    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.get(`${rooturl}/student/getstudentprofile/${data.studentid}`)
        .then(response => dispatch({
            type: GET_STUDENTPROFILE,
            payload: response.data,   
        })
            )
       
        .catch(error => {
            
            if (error.response && error.response.data) {
                return dispatch({
                    type: GET_STUDENTPROFILE,
                    payload: {
                        error : true
                    }
                });
            }
            return;
        });
}

export const updateStudentbasicdetails = (data) => dispatch => {
    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/student/updatestudentbasicdetails/${sessionStorage.getItem("studentid")}`,data)
  
        .then(response => dispatch(getStudentprofile()))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: UPDATE_STUDENTPROFILE,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const updateStudentcontactdetails = (data) => dispatch => {
    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/student/updatestudentcontactinfo/${sessionStorage.getItem("studentid")}`,data)
        .then(response => dispatch(getStudentprofile()))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: UPDATE_STUDENTPROFILE,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const updateStudentcareerobjective = (data) => dispatch => {
    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/student/updatestudentcareerobjective/${sessionStorage.getItem("studentid")}`,data)
        .then(response => dispatch(getStudentprofile()))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: UPDATE_STUDENTPROFILE,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const updateStudentskillset = (data) => dispatch => {
    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/student/updatestudentskillset/${sessionStorage.getItem("studentid")}`,data)
  
        .then(response => dispatch(getStudentprofile()))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: UPDATE_STUDENTPROFILE,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const addEducation = (data) => dispatch => {
    const data1={
        studentid:sessionStorage.getItem("studentid")
    }
    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/student/addeducation/${sessionStorage.getItem("studentid")}`,data)
  
        .then(response => dispatch(getStudentprofile(data1)))

        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: UPDATE_STUDENTPROFILE,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const updateEducation = (data) => dispatch => {
    const data1={
        studentid:sessionStorage.getItem("studentid")
    }
    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/student/updateeducation/${sessionStorage.getItem("studentid")}/`,data)
  
        .then(response => dispatch(getStudentprofile(data1)))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: UPDATE_STUDENTPROFILE,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const deleteEducation = (data) => dispatch => { 
    const data1={
    studentid:sessionStorage.getItem("studentid")
}

    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/student/deleteeducation/${sessionStorage.getItem("studentid")}/`,data)
  
        .then(response => dispatch(getStudentprofile(data1)))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: UPDATE_STUDENTPROFILE,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const addExperience = (data) => dispatch => {
    const data1={
        studentid:sessionStorage.getItem("studentid")
    }
    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/student/addexperience/${sessionStorage.getItem("studentid")}`,data)
  
        .then(response => dispatch(getStudentprofile(data1)))

        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: UPDATE_STUDENTPROFILE,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const updateExperience  = (data) => dispatch => {
    const data1={
        studentid:sessionStorage.getItem("studentid")
    }
    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/student/updateexperience/${sessionStorage.getItem("studentid")}/`,data)
  
        .then(response => dispatch(getStudentprofile(data1)))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: UPDATE_STUDENTPROFILE,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const deleteExperience  = (data) => dispatch => {
    const data1={
        studentid:sessionStorage.getItem("studentid")
    }
    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/student/deleteexperience/${sessionStorage.getItem("studentid")}/`,data)
  
        .then(response => dispatch(getStudentprofile(data1)))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: UPDATE_STUDENTPROFILE,
                    payload: error.response.data
                });
            }
            return;
        });
}

export const getCompanyprofile = (data) => dispatch => {
    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.get(`${rooturl}/company/getcompanyprofile/${data.companyid}`)
        .then(response => dispatch({
            type: GET_COMPANYPROFILE,
            payload: response.data
        }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: GET_COMPANYPROFILE,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const updateCompanybasicdetails = (data) => dispatch => {
    const data1={
        companyid:sessionStorage.getItem("companyid")
    }
    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/company/updatecompanybasicdetails/${sessionStorage.getItem("companyid")}`,data)
  
        .then(response => dispatch(getCompanyprofile(data1)))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: UPDATE_COMPANYPROFILE,
                    payload: error.response.data
                });
            }
            return;
        });
}

export const updateCompanycontactdetails = (data) => dispatch => {
    const data1={
        companyid:sessionStorage.getItem("companyid")
    }
    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/company/updatecompanycontactdetails/${sessionStorage.getItem("companyid")}`,data)
        .then(response => dispatch(getCompanyprofile(data1)))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: UPDATE_COMPANYPROFILE,
                    payload: error.response.data
                });
            }
            return;
        });
}

