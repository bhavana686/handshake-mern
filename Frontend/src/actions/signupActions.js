import { STUDENT_SIGNUP, COMPANY_SIGNUP} from "./types";
import { STUDENT_LOGIN, STUDENT_LOGOUT } from "./types";
import { COMPANY_LOGIN, COMPANY_LOGOUT } from "./types";
import {rooturl} from '../webConfig';

import axios from "axios";

export const studentSignup = (studentData) => dispatch => {
    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/student/studentsignup`, studentData)
        .then(response => dispatch({
            type: STUDENT_SIGNUP,
            payload: response.data
        }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: STUDENT_SIGNUP,
                    payload: error.response.data
                });
            }
            return;
        });
}

export const companySignup = (companyData) => dispatch => {
    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/company/companysignup`, companyData)
        .then(response => 
            {console.log(response.data);
            dispatch({
            type: COMPANY_SIGNUP,
            payload: response.data
            
        })})
        
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: COMPANY_SIGNUP,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const studentLogin = (loginData) => dispatch => {
    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/student/studentlogin`, loginData)
        .then(response => dispatch({
            type: STUDENT_LOGIN,
            payload: response.data
        }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: STUDENT_LOGIN,
                    payload: error.response.data
                });
            }
        });
}

export const studentLogout = () => dispatch => dispatch({type: STUDENT_LOGOUT});

export const companyLogin = (loginData) => dispatch => {
    axios.defaults.withCredentials = true;
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/company/companylogin`, loginData)
        .then(response => dispatch({
            type: COMPANY_LOGIN,
            payload: response.data
        }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: COMPANY_LOGIN,
                    payload: error.response.data
                });
            }
        });
}

export const companyLogout = () => dispatch => dispatch({type: COMPANY_LOGOUT});