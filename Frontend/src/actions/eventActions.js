import { GET_COMPANYEVENTLIST,GET_EVENTDETAILS, GET_EVENTSREGISTERED ,
   GET_EVENTSNOTREGISTERED,ADD_EVENT ,GET_STUDENTSAPPLIEDTOEVENT, GET_ALLEVENTS } from "./types";

import {rooturl} from '../webConfig';

import axios from "axios";

export const getCompanyEventlist = (data) => dispatch => {
    axios.defaults.withCredentials = true;
    console.log(data.companyid);
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.get(`${rooturl}/event/getallevents/${sessionStorage.getItem("companyid")}`)
        .then(response => dispatch({
            type: GET_COMPANYEVENTLIST,
            payload: response.data
        }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: GET_COMPANYEVENTLIST,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const getEventdetails = (data) => dispatch => {
    axios.defaults.withCredentials = true;
    console.log(data.eventid);
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.get(`${rooturl}/event/geteventdetails/${data.eventid}`,)
        .then(response => dispatch({
            type: GET_EVENTDETAILS,
            payload: response.data
        }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: GET_EVENTDETAILS,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const getStudentsAppliedToEvent = (data) => dispatch => {
    axios.defaults.withCredentials = true;
    console.log(data.eventid);
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/event/studentsappliedtoevent`,data)
        .then(response => dispatch({
            type: GET_STUDENTSAPPLIEDTOEVENT ,
            payload: response.data
        }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: GET_STUDENTSAPPLIEDTOEVENT ,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const addEventPost = (data) => dispatch => {
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.defaults.withCredentials = true;
    console.log(data.companyid);
    axios.post(`${rooturl}/company/addevent`,data)
        .then(response => dispatch({
            type: ADD_EVENT,
            payload: response.data
        }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: ADD_EVENT,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const getAllevents = () => dispatch => {
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/event/getallevents`)
        .then(response => dispatch({
            type: GET_ALLEVENTS ,
            payload: response.data
        }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type: GET_ALLEVENTS ,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const getEventsnotregistered = (data) => dispatch => {
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/event/eventsnotregistered`,data)
        .then(response => dispatch({
            type: GET_EVENTSNOTREGISTERED ,
            payload: response.data
        }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type:  GET_EVENTSNOTREGISTERED,
                    payload: error.response.data
                });
            }
            return;
        });
}
export const  geteventsregistered  = (data) => dispatch => {
    axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
    axios.post(`${rooturl}/event/eventsregistered`,data)
        .then(response => dispatch({
            type: GET_EVENTSREGISTERED ,
            payload: response.data
        }))
        .catch(error => {
            if (error.response && error.response.data) {
                return dispatch({
                    type:  GET_EVENTSREGISTERED,
                    payload: error.response.data
                });
            }
            return;
        });
}

