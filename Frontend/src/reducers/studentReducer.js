import { GET_ALLSTUDENTS,GET_EVENTSNOTREGISTERED,GET_EVENTSREGISTERED, APPLY_FORJOB,GET_RESUME, GET_STUDENTSAPPLIEDTOJOB , GET_ALLOTHERSTUDENTS,GET_ALLEVENTS,GET_STUDENTSAPPLIEDTOEVENT, GET_ALLJOBLIST, UPDATE_APPLICATIONSTATUS } from '../actions/types';


const initialState = {
    studentlist: [],
    studenteventlist:[],
    studentjoblist:[],
    alleventslist:[],
    eventsnotreglist:[],
    eventsreglist:[],
    jobappliedlist:[],
    resume:[],
    applicationstatus:[],
    Appliedstatus:[],
   
   
};

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_ALLSTUDENTS:
            return {
                ...state,
                studentlist: action.payload
            };
            case GET_ALLOTHERSTUDENTS:
            return {
                ...state,
                studentlist: action.payload
            };
        case GET_STUDENTSAPPLIEDTOEVENT:
                return {
                ...state,
                studenteventlist: action.payload
                };
        case GET_ALLJOBLIST:
                return {
                       ...state,
                      studentjoblist: action.payload
                    };
        case GET_ALLEVENTS:
                return {
                         ...state,
                         alleventslist: action.payload
                    };
        case GET_EVENTSNOTREGISTERED:
                return {
                         ...state,
                        eventsnotreglist: action.payload
                            
                        };
                 
        case GET_EVENTSREGISTERED:
                return {
                         ...state,
                        eventsreglist: action.payload
                                        
                        };
         case GET_STUDENTSAPPLIEDTOJOB:
                return {
                         ...state,
                         jobappliedlist: action.payload
                                                    
                         };
        case GET_RESUME:
                return {
                         ...state,
                         resume: action.payload
                                                                
                        };
        case UPDATE_APPLICATIONSTATUS:
                            return {
                                     ...state,
                                     applicationstatus: action.payload
                                                                            
                                    };
        case APPLY_FORJOB:
                            return {
                                     ...state,
                                     Appliedstatus: action.payload
                                                                                        
                                    };
                   
                             
                    
        default:
            return state;
    }
};


 
 