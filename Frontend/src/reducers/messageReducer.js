import {  GET_RECEIVERS ,ADD_MESSAGE} from '../actions/types';



 const initialState = {
     conversation: {},
     newmsg:[],
 };

 export default function(state = initialState, action){
    switch(action.type){
        case GET_RECEIVERS:
            return {
                ...state,
                conversation: action.payload
            };
        case ADD_MESSAGE:
            return {
                ...state,
                newmsg: action.payload
            };
        
            default:
                return state;
        }
        
        
 };