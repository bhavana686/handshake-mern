import { STUDENT_LOGIN, STUDENT_LOGOUT } from '../actions/types';
import { COMPANY_LOGIN, COMPANY_LOGOUT } from '../actions/types';


 const initialState = {
     user: {}
 };

 export default function(state = initialState, action){
    switch(action.type){
        case STUDENT_LOGIN:
            return {
                ...state,
                user: action.payload
            };
        case STUDENT_LOGOUT:
            return {};
           
        case COMPANY_LOGIN:
            return {
                ...state,
                user: action.payload
            };
        case COMPANY_LOGOUT:
            return {};
            default:
                return state;
        }
        
        
 };