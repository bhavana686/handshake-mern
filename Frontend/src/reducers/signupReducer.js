import { STUDENT_SIGNUP, COMPANY_SIGNUP } from '../actions/types';




const initialState = {
    user: {}
};

export default function (state = initialState, action) {
    switch (action.type) {
        case STUDENT_SIGNUP:
            return {
                ...state,
                user: action.payload
            };
        case COMPANY_SIGNUP:
            return {
                ...state,
                user: action.payload
            };
        default:
            return state;
    }
};


 
 