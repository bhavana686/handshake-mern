import { GET_COMPANYPROFILE,GET_STUDENTPROFILE } from '../actions/types';

import { UPDATE_COMPANYPROFILE,UPDATE_STUDENTPROFILE } from '../actions/types';




const initialState = {
    profile: {}
};

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_STUDENTPROFILE:
            return {
                ...state,
                profile: action.payload
            };
        case GET_COMPANYPROFILE:
            return {
                ...state,
                profile: action.payload
            };
        case UPDATE_STUDENTPROFILE:
            return {
                ...state,
                profile: action.payload
            };
        case UPDATE_COMPANYPROFILE:
            return {
                ...state,
                profile: action.payload
            };
        default:
            return state;
    }
};


 
 