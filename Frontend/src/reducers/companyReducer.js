import { GET_COMPANYJOBLIST,ADD_JOB, GET_EVENTDETAILS } from '../actions/types';
import { GET_COMPANYEVENTLIST, ADD_EVENT } from '../actions/types';


const initialState = {
    companyjoblist: [],
    companyeventlist:[],
    eventdetails:{},
    resume:[]
    
};

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_COMPANYJOBLIST:
            return {
                ...state,
                companyjoblist: action.payload
            };
        case ADD_JOB:
            return {
                ...state,
                companyjoblist: state.companyjoblist.concat(action.payload)
            };
        case GET_COMPANYEVENTLIST:
            return {
                ...state,
                companyeventlist: action.payload
                };
        case ADD_EVENT:
            return {
                ...state,
                companyeventlist: state.companyeventlist.concat(action.payload)
                };
        case GET_EVENTDETAILS:
            return {
                 ...state,
                 eventdetails: action.payload
                };
           
        default:
            return state;
    }
};


 
 