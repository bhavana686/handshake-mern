import { combineReducers } from 'redux';

import signupReducer from './signupReducer';
import loginReducer from './loginReducer';
import profileReducer from './profileReducer';
import companyReducer from './companyReducer';
import studentReducer from './studentReducer';
import messageReducer from './messageReducer';


export default combineReducers({
    signup: signupReducer,
    login: loginReducer,
    profile:profileReducer,
    companyjoblist:companyReducer,
    studentlist:studentReducer,
    conversation:messageReducer,
    
});