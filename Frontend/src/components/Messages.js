import React, { Component } from "react";
//import '../../App.css';
import axios from "axios";

import { rooturl } from "../webConfig";
import "./Messages.css";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Avatar } from "@material-ui/core";
import { getFriends} from '../actions/messageActions';

class Messages extends Component {
    constructor(props) {
        super(props);
        this.state = {
            messagecontent: "",
            msglist: {},
            profilepic: "",
            currentchat: [],
            temp: " ",
        };

        // console.log(props.location.state)
        this.displaychatofperson = this.displaychatofperson.bind(this);
        this.messagecontentChangeHandler = this.messagecontentChangeHandler.bind(this);
        this.getreceivers = this.getreceivers.bind(this);
    }

    componentDidMount() {
        console.log("####");

        this.getreceivers();
    }
    getreceivers = () => {
        const data = {
            userid: sessionStorage.getItem("studentid")
                ? sessionStorage.getItem("studentid")
                : sessionStorage.getItem("companyid"),
            usertype: sessionStorage.getItem("loggintype"),
        };
        console.log(data);
        this.props.getFriends(data)
        axios.post(`${rooturl}/allreceivers`, data).then((response) => {
            console.log(response.data);
            console.log(this.state.temp);
            var sid = this.props.location.state
                ? this.props.location.state.studentlist
                    ? this.props.location.state.studentlist
                    : ""
                : "";
            if (response.data != null && sid.studentid != null) {
                console.log("2");
                if (response.data.conList) {
                    console.log("3");
                    var c = 0;
                    for (var i = 0; i < response.data.conList.length; i++) {
                        if (sid.studentid === response.data.conList[i].id) {
                            console.log("4");
                            this.setState({
                                msglist: response.data,
                            });
                            this.displaychatofperson(i);
                        } else {
                            c++;
                        }
                    }
                    if (c === response.data.conList.length) {
                        let addothermsg = {};
                        console.log(sid);
                        addothermsg = {
                            studentid: sid.studentid,
                            name: sid.name,
                            collegename: sid.collegename ? sid.collegename : sid.companylocation,
                            profilepic: sid.profilepic,
                        };
                        console.log("5");
                        this.setState({
                            msglist: response.data.conList.push(addothermsg),
                        });
                        this.displaychatofperson(c);
                    }
                }
            } else {
                this.setState({
                    msglist: response.data,
                });
                this.displaychatofperson(this.state.temp);
            }
            this.setState({
                msglist: response.data,
            });
        });
    };

    submitMsg = (e) => {
        var sid = this.props.location.state
            ? this.props.location.state.studentlist
                ? this.props.location.state.studentlist
                : ""
            : "";
        console.log(sid);
        if (sessionStorage.getItem("loggintype") === "student") {
            const data = {
                messagecontent: this.state.messagecontent,
                msgid: this.state.currentchat ? this.state.currentchat.msgid : "",
                user1id: sessionStorage.getItem("studentid"),
                user1type: "student",
                user2id: this.state.currentchat.id
                    ? this.state.currentchat.id
                    : this.state.currentchat.studentid,
                user2type: "student",
            };

            console.log(data.user1id);
            console.log(data.user2id);
            console.log(this.state.currentchat.studentid);
            console.log(this.state.currentchat.id);

            console.log(
                this.state.currentchat.studentid === "undefined"
                    ? this.state.currentchat.id
                    : this.state.currentchat.studentid
            );
            axios.post(`${rooturl}/addmessage`, data).then((response) => {
                console.log(response.data);
                this.getreceivers();
            });
            this.setState({
                messagecontent: "",
            });





        } else {
            const data = {
                messagecontent: this.state.messagecontent,
                msgid: this.state.currentchat ? this.state.currentchat.msgid : "",
                user1id: sessionStorage.getItem("companyid"),
                user1type: "company",
                user2id:
                    this.state.currentchat.companyid === "undefined"
                        ? this.state.currentchat.id
                        : this.state.currentchat.studentid,
                user2type: this.state.currentchat.companyid ? "company" : "student",
            };

            axios.post(`${rooturl}/addmessage`, data).then((response) => {
                console.log(response.data);
                this.getreceivers();
            });
            this.setState({
                messagecontent: "",
            });
        }
    };
    messagecontentChangeHandler = (e) => {
        this.setState({
            messagecontent: e.target.value,
        });
    };
    displaychatofperson = (index) => {
        console.log(index);
        this.setState({
            currentchat: this.state.msglist
                ? this.state.msglist.conList
                    ? this.state.msglist.conList[index]
                    : ""
                : "",
            temp: index,
        });
    };

    render() {
        let currentchatdisplay = null;
        let sendbox = null;
        let friendslist = null;
        let namedisplay = null;
        friendslist = this.state.msglist
            ? this.state.msglist.conList
                ? this.state.msglist.conList.map((msglist, index) => {
                    console.log("hi")
                    //console.log(msglist.companyprofilepic?msglist.companyprofilepic.profilepic?msglist.companyprofilepic.profilepic:"":"")
                    return (
                        <div
                            class="form-group row"
                            onClick={() => this.displaychatofperson(index)}
                            paddingleft
                        >
                            <div class="col-lg-4">
                                <Avatar
                                    variant="circle"
                                    src={msglist.profilepic ? msglist.profilepic : msglist.companyprofilepic ?
                                        msglist.companyprofilepic.profilepic ? msglist.companyprofilepic.profilepic : "" : ""}
                                    style={{
                                        cursor: "pointer",
                                        width: "90px",
                                        height: "90px",
                                        margin: "15px",
                                        border: "0.5px solid",
                                    }}
                                />
                            </div>
                            <div class="col-lg-2"></div>
                            <div class="col-lg-4">
                                <h3>{msglist.name}</h3>{" "}
                                <h3>
                                    {msglist.collegename ? msglist.collegename : msglist.location}
                                </h3>
                            </div>
                        </div>
                    );
                })
                : ""
            : "";
        sendbox = (
            <div class="card-body">
                <div class="form-group">
                    <input
                        type="textarea"
                        class="form-control"
                        name="messagebox"
                        value={this.state.messagecontent}
                        onChange={this.messagecontentChangeHandler}
                        placeholder="enter message"
                        required
                    />
                </div>
                <button onClick={this.submitMsg} type="submit" class="btn btn-primary">
                    send
        </button>
                <br />
                <br />
            </div>
        );

        if (this.state.currentchat) {
            console.log(this.state.currentchat);
            console.log(this.state.currentchat.studentid);
            namedisplay = this.state.currentchat
                ? this.state.currentchat.name
                    ? this.state.currentchat.name
                    : ""
                : "";
            console.log(namedisplay);

            currentchatdisplay = this.state.currentchat
                ? this.state.currentchat.message
                    ? this.state.currentchat.message.map((mlist) => {
                        if (
                            mlist.userid === sessionStorage.getItem("studentid") ||
                            mlist.userid === sessionStorage.getItem("companyid")
                        ) {
                            console.log("1");
                            return (
                                <div class="container1 darker">
                                    <img
                                        src={
                                            this.props.profile ? this.props.profile.profilepic : ""
                                        }
                                        alt="Avatar"
                                        class="right"
                                        style={{ width: "100%" }}
                                    />
                                    <p>{mlist.messagecontent}</p>
                                </div>
                            );
                        } else {
                            console.log("2");
                            return (
                                <div class="container1">
                                    <img

                                        src={
                                            this.state.currentchat.profilepic ? this.state.currentchat.profilepic : this.state.currentchat.companyprofilepic ? this.state.currentchat.companyprofilepic.profilepic ? this.state.currentchat.companyprofilepic.profilepic : "" : ""}
                                        alt="Avatar"
                                        style={{ width: "100%" }}
                                    />
                                    <p>&nbsp;&nbsp;&nbsp;{mlist.messagecontent}</p>
                                </div>
                            );
                        }
                    })
                    : ""
                : "";
        }
        return (
            <div class="paddingleft15">
                <div class="form-group row" paddingleft>
                    <div class="col-lg-1"></div>
                    <div class="col-lg-3">
                        <div class="well">{friendslist}</div>
                    </div>

                    <div class="col-lg-7">
                        <div class="well">
                            <h3>Messages</h3>
                            <h1>{namedisplay}</h1>
                            {currentchatdisplay}
                            {sendbox}
                        </div>
                    </div>
                    <div class="col-lg-1"></div>
                </div>
            </div>
        );
    }
}

Messages.propTypes = {
    getFriends: PropTypes.func.isRequired,
    profile: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
    profile: state.profile.profile,
    conversation:state.conversation.conversation,
});
export default connect(mapStateToProps,{ getFriends })(Messages);

