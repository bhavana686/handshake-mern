import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import {rooturl} from '../webConfig';
import Companylandingpage from './Company/Jobs/Companylandingpage';
import Addjobpage from './Company/Jobs/Addjobpage'
import CompanySignup from './CompanySignup';
import StudentSignup from './StudentSignup';
import StudentLogin from './StudentLogin';
import CompanyLogin from './CompanyLogin';
import Addevent from './Company/Event/Addevent';
import Navbar from './Navbar';
import Studentappliedtoevent from './Company/Event/Studentappliedtoevent';

import Studentappliedtojob from './Company/Jobs/Studentappliedtojob';
import Companydisplayprofile from './Company/Companyprofile/Companydispalyprofile';
import Studentlandingpage from  './Company/Studentlandingpage';
import Applicationpage from  './Company/Applicationpage';
import Studenteventpage from  './Student/Event/Studenteventpage';



import Eventsearch from './Student/Event/Eventsearch';
import Registerevent from './Student/Event/Registerevent';
import Companyeventpage from './Company/Event/Companyeventpage';
import Studentpage from './Student/Studentpage';

import Studentprofile from './Student/Studentprofile/Studentprofile';
import Studentprofileout from './Student/Studentprofile/Studentprofileout';

import Jobsearch from './Student/Jobsearch';
import Viewevent from './Student/Event/Viewevent';
import Companyviewevent from './Company/Event/Companyviewevent';
import Companyprofileout from './Company/Companyprofile/Companyprofileout';
import Messages from '../components/Messages';



class Main extends Component {
    render() {
        return (
            <div>
                 <Route path="/" component={Navbar}/>
                <Route path="/StudentSignup" component={StudentSignup}/>
                <Route path="/StudentLogin" component={StudentLogin}/>
                <Route path="/CompanySignup" component={CompanySignup}/>
                <Route path="/CompanyLogin" component={CompanyLogin}/>
                <Route path="/Companylandingpage" component={Companylandingpage}/>
                <Route path="/Studentlandingpage" component={Studentlandingpage}/>
                <Route path="/Companyviewevent/:id" component={Companyviewevent}/>
                <Route path="/Addjobpage" component={Addjobpage}/>
                <Route path="/Companydisplayprofile/:id" component={Companydisplayprofile}/>
                <Route path="/Studentappliedtojob/:id" component={Studentappliedtojob}/>
                <Route path="/Studentappliedtoevent/:id" component={Studentappliedtoevent}/>
                <Route path="/Applicationpage" component={Applicationpage}/>
                <Route path="/Studenteventpage" component={Studenteventpage}/>
                <Route path="/Addevent" component={Addevent}/>
                <Route path="/Messages" component={Messages}/>
                    <Route path="/Eventsearch" component={Eventsearch}/>
                    <Route path="/Registerevent" component={Registerevent}/>
                    <Route path="/Companyeventpage" component={Companyeventpage}/>
                    <Route path="/Studentpage" component={Studentpage}/>
                   
                    <Route path="/Studentprofile/:id" component={Studentprofile}/>
                    <Route path="/Studentprofileout/:id" component={Studentprofileout}/>
                    <Route path="/Student/Jobsearch" component={Jobsearch}/>
                    <Route path="/Viewevent/:id" component={Viewevent}/>
                    <Route path="/Companyprofileout/:id" component={Companyprofileout}/>



            </div>
        )
    }
}
export default Main;