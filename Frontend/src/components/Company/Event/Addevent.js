import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';

import { Link } from 'react-router-dom';

import { addEventPost } from '../../../actions/eventActions'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';


class Addevent extends Component {

    constructor(props) {
        //Call the constrictor of Super class i.e The Component
        super(props);
        //maintain the state required for this component
        this.state = {
            eventname:"",
            eventdescription: "",
            eventtime: "",
            eventdate: "",
            eventlocation: "",
            eventeligibility: "",
            authFlag:""
        }
        //Bind the handlers to this class
       
      
        this.onChange = this.onChange.bind(this);




    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
  
    submitEvent = (e) => {

       
        e.preventDefault();

        const data = {
            
            eventname:this.state.eventname,
            eventdescription: this.state.eventdescription,
            eventtime: this.state.eventtime,
            eventdate: this.state.eventdate,
            eventlocation: this.state.eventlocation,
            eventeligibility: this.state.eventeligibility,
            companyid:sessionStorage.getItem('companyid')
        }
        console.log(data);
        //set the with credentials to true
        axios.defaults.withCredentials = true;
        console.log(data);
        this.props.addEventPost(data)
        //set the with credentials to true
       
                    this.setState({
                        authFlag: 1
                    })
       
        
    }



    render() {
        let redirectval = null;
        let random = null;
        if (sessionStorage.getItem('loggintype') !== 'company') {
            redirectval = <Redirect to='/CompanySignup' />
        }
        if (this.state.authFlag === 1)
            redirectval = <Redirect to ='/Companyeventpage' />
        else if (this.state.authFlag === 2)
            random = <p> could not add event </p>
        else{
            random = null;

        }

        return (
            <div>
                {redirectval}
                <br />
                <form onSubmit={this.submitPost}>
                    <div class="container">


                        <div class="row">
                            <div class="col-lg-10 col-xl-9 mx-auto">
                                <div class="card card-signin flex-row my-5">

                                    <div class="card-body">
                                        <h5 class="card-title text-center"> POST A EVENT</h5>
                                    </div>

                                    <div class="form-label-group">
                                        <label class="control-label col-sm-2" for="eventname">Event name:</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="eventname" id="eventname" onChange={this.onChange} class="form-control" placeholder="Event name" required />
                                        </div>
                                    </div><br/><br/>
                                    <div class="form-label-group">
                                        <label class="control-label col-sm-2" for="eventdescription">Event Description:</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="eventdescription" id="eventdescription" onChange={this.onChange} class="form-control" placeholder="Event description" required />
                                        </div>
                                    </div><br/><br/>
                                    <div class="form-label-group">
                                    <label class="control-label col-sm-2" for="eventtime">Event Time:</label>
                                    <div class="col-sm-10">

                                    
                                            <input type="text" name="eventtime" id="eventtime" onChange={this.onChange} class="form-control" placeholder="Event Time" required />
                                        </div>
                                    </div><br/><br/>
                                    <div class="form-label-group">
                                        <label class="control-label col-sm-2" for="eventdate">Event Date:</label>
                                        <div class="col-sm-10">
                                        <input type="date" name="eventdate" id="eventdate" onChange={this.onChange} class="form-control" placeholder="Event Date" required />
                                </div>
                                </div><br /><br />
                                <div class="form-label-group">
                                    <label class="control-label col-sm-2" for="eventlocation">Event location:</label>
                                    <div class="col-sm-10">
                                    <input type="text" name="eventlocation"  id="eventlocation" onChange={this.onChange} class="form-control" placeholder="Event location" required />
                                </div>
                            </div><br /><br />
                            
                            <div class="form-label-group">
                                <label class="control-label col-sm-2" for="eventeligibility">Event Eligibility:</label>
                                <div class="col-sm-10">
                                <input type="text" name="eventeligibility" id="eventeligibility" onChange={this.onChange} class="form-control" placeholder="Event Eligibility" required />
                            </div><br /><br />
                        </div>
                    <br /><br />
                    <div class="form-group">
                    <button onClick={this.submitEvent} class="btn btn-success" type="submit">Add</button>&nbsp;

                      
                        <Link to="/Companyeventpage"><button type="submit" class="btn btn-success">Cancel</button></Link>

                    </div>
                    {random}
        </div>
        </div>
        </div>
        </div>
        
        </form>
        </div>
    
        )
    }
}

Addevent.propTypes = {
    
    addEventPost: PropTypes.func.isRequired,

    
};



export default connect(null , { addEventPost  })(Addevent);
