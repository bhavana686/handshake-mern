import React, { Component } from "react";
import { Link } from 'react-router-dom';
import {getCompanyEventlist} from '../../../actions/eventActions'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TablePagination from '@material-ui/core/TablePagination';







class Companyeventpage extends Component {
    constructor(props) {

        super(props);
        this.state =
        {
            
            companyeventlist: [],
            page:0,
            rowsPerPage:5,
            

        }


    }



    componentDidMount() {

        const data = { companyid : sessionStorage.getItem('companyid')  }
        console.log(data);
        this.props.getCompanyEventlist(data);
           

    }

    handleChangePage = (event, newPage) => {
        this.setState({
            page: newPage
        })
       };
       handleChangeRowsPerPage = (event) => {
        this.setState({ 
            rowsPerPage: (parseInt(event.target.value, 10)),
            page: 0,
         })
        
    
      };
    

    render() {
        let displayform=null;
        console.log(this.props.companyeventlist.length);
        if(this.props.companyeventlist.length>0)
            {
                displayform =  (


            
         
           
                (this.state.rowsPerPage > 0
                    ?  this.props.companyeventlist.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage)
                    : this.props.companyeventlist
                  ).map((Eventdetails,index)=> {
            
              
              return(
                
            
                    <div>

                       
                                    <div class="well" text-right>
                                        <h5>Event Title:   {Eventdetails.eventname}</h5><br></br>
                                        <Link to={"/Companyviewevent/"+Eventdetails.eventid}   className="btn btn-primary">View Details</Link>&nbsp;&nbsp;
                                        <Link to={"/Studentappliedtoevent/"+Eventdetails.eventid}   className="btn btn-primary">Students Registered</Link>
                        
                                   
                                                
                                            </div>
                                        </div>
                             )
                            }))
                              }
    return(
            <div>
            

    <div class="paddingleft15">
        <div class="form-group row" paddingleft>

            <div class="col-lg-10"> </div>
            <div class="col-lg-1"><Link to={"/Addevent"} className="btn btn-primary">Add Event</Link> </div>
        </div>

        <div class="form-group row" paddingleft>
            <div class="col-lg-3"></div>
            <div class="col-lg-8">  <h3>List of Events posted by company</h3>
              {displayform}
              <h1><TablePagination 
                            style={{ fontSize: 10 }}
                          rowsPerPageOptions={[5, 10, 25]}
                          component="div"
                          count={this.props.companyeventlist.length}
                          rowsPerPage={this.state.rowsPerPage}
                          page={this.state.page}
                          onChangePage={this.handleChangePage}
                          onChangeRowsPerPage={this.handleChangeRowsPerPage}/></h1>
                    
            
            </div>

        </div>

    </div>
    </div>
       
       
       
            
        );
    }

}

Companyeventpage.propTypes = {
    
    getCompanyEventlist: PropTypes.func.isRequired,

};

const mapStateToProps = state => ({
    companyeventlist: state.companyjoblist.companyeventlist,
});

export default connect(mapStateToProps, { getCompanyEventlist  })(Companyeventpage);
