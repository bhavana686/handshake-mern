
import React, { Component } from "react";

import { Link } from 'react-router-dom';

import { getStudentsAppliedToEvent } from '../../../actions/eventActions'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';





class Studentappliedtoevent extends Component {
    constructor(props) {

        super(props);
        this.state =
        {
            
          
        }
    }

    
    componentWillMount() {

        const { match: { params } } = this.props
        const data = {
            eventid: params.id

        }
        this.props.getStudentsAppliedToEvent(data)
       
    }
   


    render() {
        let displayform=null;
        console.log( this.props.studenteventlist)
       
    if(this.props.studenteventlist.length>0)
    {
         displayform = this.props.studenteventlist[0].registrations.map((studentdetails, index) => {
            
                return(
                  
                      <div>
                          <div class="well" text-right>
                          <h5>Student name:{studentdetails.name}</h5>
                            <h5>Student mail:   {studentdetails.mail}</h5>
                   
                              <div><Link to={"/Studentprofileout/"+studentdetails.studentid} className="btn btn-primary">View profile</Link> </div>
                
                          </div>
                          </div>
                   
                )
            })
        }
            
            
      
            return (
                   <div>
          
                      <div class="paddingleft15">
                            
                          <div class="form-group row" paddingleft>
                              <div class="col-lg-2"></div>
                              <div class="col-lg-9">  <h2>List of students applied to event </h2>
                              
                              
                          
                              {displayform}
                              </div>
                              
                              </div>
                  
                  </div>
                  </div>
                 
                 
                 
              )
          }
        }




//export Home Component
Studentappliedtoevent.propTypes = {
    
    getStudentsAppliedToEvent: PropTypes.func.isRequired,

};

const mapStateToProps = state => ({
    studenteventlist: state.studentlist.studenteventlist,
});

export default connect(mapStateToProps, { getStudentsAppliedToEvent})(Studentappliedtoevent);
