
import React, { Component } from 'react';
//import '../../App.css';

import dateFormat from 'dateformat';

import { getEventdetails } from '../../../actions/eventActions'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';





class Companyviewevent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            eventdetails: {},
         
           
        }
       
        
    }
 
    
 
    componentDidMount() {

        const { match: { params } } = this.props
        const data = {
            eventid: params.id

        }
        console.log(data.eventid)
        console.log(data);
        this.props.getEventdetails(data);
        
           
           
              
    
    }
    


    render() {
        
         console.log(this.props.eventdetails);
         console.log(this.props.eventdetails[0]);
        

        let details =  
             (<div>
                <div class="well" text-right>
                    <h5>name:{this.props.eventdetails[0]?this.props.eventdetails[0].eventname:""}</h5>
                    <h5>description:{this.props.eventdetails[0]?this.props.eventdetails[0].eventdescription:""}</h5>
                    <h5> time:{this.props.eventdetails[0]?this.props.eventdetails[0].eventtime:""}</h5>
                    <h5> date:{dateFormat(this.props.eventdetails[0]?this.props.eventdetails[0].eventdate:"" ,"mmmm dS, yyyy")}</h5>
                    <h5> location:{this.props.eventdetails[0]?this.props.eventdetails[0].eventlocation:""}</h5> 
                    <h5> eligibility:{this.props.eventdetails[0]?this.props.eventdetails[0].eventeligibility:""}</h5>




                   
                        
            </div>
            </div>

            )
        
        return (
            <div>
                
                <div class="paddingleft15">
                    
                      
                    <div class="form-group row" paddingleft>
                        <div class="col-lg-2"></div>
                        <div class="col-lg-9">  <h2>Event Details</h2>
                        
                

                            

                            {details}

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
//export Home Component
Companyviewevent.propTypes = {
    
    getEventdetails: PropTypes.func.isRequired,

};

const mapStateToProps = state => ({
    eventdetails: state.companyjoblist.eventdetails,
});

export default connect(mapStateToProps, { getEventdetails})(Companyviewevent);
