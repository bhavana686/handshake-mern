import React, { Component } from 'react';
import axios from 'axios';

import dateFormat from 'dateformat';
import { rooturl } from '../../webConfig';
import TablePagination from '@material-ui/core/TablePagination';

class Applicationpage extends Component {
    constructor() {
        super();
        this.state = {
            studentlist: [],
            msg: "",
            tempapplicationstatus: "",
            filterlist: [],
            filtercheck: false,
            page: 0,
            rowsPerPage: 5,



        }
        this.changestatus = this.changestatus.bind(this);
        this.applyfilter = this.applyfilter.bind(this);


    }

    //get the books data from backend  
    componentDidMount() {

        const data = {
            studentid: sessionStorage.getItem('studentid')

        }
        console.log(data);
        axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
        axios.post(rooturl + '/student/getstudentapplicationjobs', data)
            .then((response) => {
                console.log(response)
                console.log("fetch jobs")
                console.log(response)
                //update the state with the response data
                if (response.data.length > 0) {
                    this.setState({
                        studentlist: response.data,
                        filterlist: []

                    });
                }
                else {
                    this.setState({
                        msg: "Have not applied to anyjobs yet"

                    });
                }
            });
    }
    changestatus = (e) => {
        this.setState(
            {
                tempapplicationstatus: e.target.value
            }
        );

    }
    applyfilter = (e) => {
        var resultlist = []
        var filter = e.target.value
        console.log(filter);
        if (filter === "select") {
            this.setState(
                {

                    filtercheck: false
                });

        }
        else {
            console.log(this.state.studentlist[0].applications[0].applicationstatus)
            for (var i = 0; i < this.state.studentlist.length; i++) {
                if (this.state.studentlist[i].applications.length > 0) {
                    console.log(this.state.studentlist[i].applications[0].applicationstatus)
                    if ((this.state.studentlist[i].applications[0].applicationstatus ? this.state.studentlist[i].applications[0].applicationstatus.toLowerCase() : "notapplied") === filter.toLowerCase()) {
                        resultlist.push(this.state.studentlist[i])
                    }
                }


            }
            this.setState(
                {
                    filterlist: resultlist,
                    filtercheck: true
                }
            );
        }
    }

    handleChangePage = (event, newPage) => {
        this.setState({
            page: newPage
        })
    };
    handleChangeRowsPerPage = (event) => {
        this.setState({
            rowsPerPage: (parseInt(event.target.value, 10)),
            page: 0,
        })


    };

    render() {
        let details;
        console.log(this.state.filterlist)
        if (this.state.filtercheck)
         {
            details = (
                (this.state.rowsPerPage > 0
                    ? this.state.filterlist.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage)
                    : this.state.filterlist
                ).map((filterlist, index) => {

                    return (
                        <div class="well" text-right>
                            <h5>Job title :  {filterlist.jobtitle}</h5>
                            <h5>Application date :  {dateFormat(filterlist.applicationdate, "mmmm dS, yyyy")}</h5>
                            <h5>Appllication status :{filterlist.applications[0] ? filterlist.applications[0].applicationstatus : ""} </h5>


                        </div>
                    )}
                ))
                    
        
            


    console.log(this.state.filterlist)

}
        else
{
    details = (<div>
        {this.state.studentlist.map((studentlist, index) => {
            return (
                <div class="well" text-right>
                    <h5>Job title :  {studentlist.jobtitle}</h5>
                    <h5>Application date :  {dateFormat(studentlist.applicationdate, "mmmm dS, yyyy")}</h5>
                    <h5>Appllication status :  {studentlist.applications[0].applicationstatus}</h5>

                </div>
            )
        })}
    </div>
    )





}


//if not logged in go to login page
return (
    <div class="paddingleft15">
        <div class="form-group row">
            <div class="col-lg-1"></div>
            <div class="col-lg-3">
                <div class="well" text-right>
                    <h2>Filters</h2><br></br>
                    <h3>Application Status</h3><br></br>
                    <div>

                        <select id="" onChange={this.applyfilter}>
                            <option>select</option>
                            <option >Pending</option>
                            <option>Reviewed</option>
                            <option>Declined</option>
                        </select><br></br>
                        <p></p>
                    </div>

                </div>

            </div>
            <div class="col-lg-7">
                {details}

                <h2>{this.state.msg}</h2>
                <h1><TablePagination
                    style={{ fontSize: 10 }}
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={this.state.studentlist.length}
                    rowsPerPage={this.state.rowsPerPage}
                    page={this.state.page}
                    onChangePage={this.handleChangePage}
                    onChangeRowsPerPage={this.handleChangeRowsPerPage} /></h1>


            </div>

        </div>



    </div>
)
    }
}
//export Home Component
export default Applicationpage;