import React, { Component } from 'react';
//import '../../App.css';

import IconButton from '@material-ui/core/IconButton';
import {Edit} from '@material-ui/icons';
import Companyprofilecard from './Companyprofilecard';
import {getCompanyprofile, updateCompanybasicdetails, updateCompanycontactdetails} from './../../../actions/profileActions'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';



export class Companydisplayprofile extends Component {
    constructor(props) {
        
        super(props);
        
            

        
       
        this.state = {
            
            authFlag: 0,
            editbasic: false,
            editcontact: false,
            companyname: "",
            companylocation: "",
            companydescription: "",
            companymail: "",
            companyphonenumber: "",
        }

        this.handlebasicedit = this.handlebasicedit.bind(this);
        this.handlecontactedit = this.handlecontactedit.bind(this);
        this.changeHandler = this.changeHandler.bind(this);
        this.onChange = this.onChange.bind(this);
    }
    componentWillReceiveProps(nextProps) {
        
        if (nextProps.profile) {
            var { profile } = nextProps;

            var userData = {
                editbasic: false,
                editcontact: false,
                companyname:  profile?profile.companyname :""|| this.state.companyname,

                  companylocation: profile?profile.companylocation :""|| this.state.companylocation,

                   companydescription: profile?profile.companydescription :"" || this.state.companydescription,

                     companymail: profile?profile.companymail :"" || this.state.companymail,

                    companyphonenumber: profile?profile.companyphonenumber :"" || this.state.companyphonenumber,

               

            };

            this.setState(userData);
        }
    }
    componentDidMount() {
        const { match: { params } } = this.props
        const data = {
            companyid: params.id,


        }

        sessionStorage.setItem('companyid', data.companyid)
        console.log(data);
        this.props.getCompanyprofile(data);
     



    }
    submitbasicEdit = (e) => {

       
        e.preventDefault();

        const data = {
            companyid: sessionStorage.getItem('companyid'),
            companyname: this.state.companyname,
            companylocation: this.state.companylocation,
            companydescription: this.state.companydescription,

        }
        console.log(data);
      
        console.log(this.props.profile)
        this.props.updateCompanybasicdetails(data);
                    this.setState({
                        authFlag: 1,
                        editbasic: false

                    })
    }
    submitcontactEdit = (e) => {

     
       
        e.preventDefault();

        const data = {
            companyid: sessionStorage.getItem('companyid'),
            companymail: this.state.companymail,
            companyphonenumber: this.state.companyphonenumber,


        }
        console.log(data);
        this.props.updateCompanycontactdetails(data);
    
                    this.setState({
                        authFlag: 1,
                        editcontact: false

                    })

    }
    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handlebasicedit = () => {
        this.setState({
            editbasic: !this.state.editbasic,
        
            

        })
    }
    handlecontactedit = () => {
        this.setState({
            editcontact: !this.state.editcontact,  
        
           

        })
    }

    changeHandler = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }



    render() { 
        let editbasicform = null;
        let editcontactform = null;
        let details1=null;
        let details2=null;
       
       
        
     if (this.props.profile!=null) {
        details1 =
            (
                 <div>
                    <br></br>
                    <h4> Name:{this.props.profile?this.props.profile.companyname:""}</h4>
                    <h4>Location:{this.props.profile?this.props.profile.companylocation:""}</h4>
                    <h4>Description:{this.props.profile?this.props.profile.companydescription:""}</h4>
                   


                </div>
            )
       

        details2 =
            (
                <div>


                    <br></br>
                    <h4> Mail:{this.props.profile?this.props.profile.companymail:""}</h4>
                    <h4>Phonenumber: {this.props.profile?this.props.profile.companyphonenumber:""}</h4>



                </div>
            )

         }

        if (this.state.editbasic) {
            details1 = null;

            editbasicform =

                (
                    <div>



                        <form onSubmit={this.submitPost}>
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-10 col-xl-9 mx-auto">
                                        <div class="card card-signin flex-row my-5">
                                            <div class="form-label-group">
                                                <label class="control-label col-sm-2" for="companyname"> Company Name:</label>
                                                <div class="col-sm-2">
                                                    <input type="text" name="companyname" id="companyname" value={this.state.companyname} onChange={this.onChange} class="form-control" required />

                                                </div><br /><br />
                                                <div class="form-label-group">
                                                    <label class="control-label col-sm-2" for="companylocation">Location</label>
                                                    <div class="col-sm-2">
                                                        <input type="text" name="companylocation" id="companylocation" value={this.state.companylocation} onChange={this.onChange} class="form-control" required />
                                                    </div>
                                                </div><br /><br />
                                                <div class="form-label-group">
                                                    <label class="control-label col-sm-2" for="companydescription">Description:</label>
                                                    <div class="col-sm-2">


                                                        <input type="text" name="companydescription" id="companydescription" value={this.state.companydescription} onChange={this.onChange} class="form-control" required />
                                                    </div>
                                                </div><br /><br />

                                                <div class="form-group">
                                                    <button onClick={this.submitbasicEdit} class="btn btn-success" type="submit">Save</button>&nbsp;
                                                  <button onClick={this.handlebasicedit} class="btn btn-success" type="submit">Cancel</button>&nbsp;
      </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>


                );
        }







        if (this.state.editcontact) {
            details2 = null;

            editcontactform =

                (
                    <div>



                        <form onSubmit={this.submitPost}>
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-10 col-xl-9 mx-auto">
                                        <div class="card card-signin flex-row my-5">
                                            <div class="form-label-group">
                                                <label class="control-label col-sm-2" for="companyphonenumber"> Phone Number:</label>
                                                <div class="col-sm-2">
                                                    <input type="text" name="companyphonenumber" id="companyphonenumber" value={this.state.companyphonenumber} onChange={this.onChange} class="form-control" required />

                                                </div><br /><br />
                                                <div class="form-label-group">
                                                    <label class="control-label col-sm-2" for="companymail">Mail</label>
                                                    <div class="col-sm-2">
                                                        <input type="email" name="companymail" id="companymail" value={this.state.companymail} onChange={this.onChange} class="form-control" required />
                                                    </div>
                                                </div><br /><br />
                                                

                                                <div class="form-group">
                                                    <button onClick={this.submitcontactEdit} class="btn btn-success" type="submit">Save</button>&nbsp;
                                                    <button onClick={this.handlecontactedit} class="btn btn-success" type="submit">Cancel</button>&nbsp;
        </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>


                );
        }
        if(sessionStorage.getItem('loggintype')==="student")
        {
            editbasicform=null;
            editcontactform=null;

        }
        return (
            <div>

                <div class="paddingleft15">


                    <div class="form-group row" paddingleft>
                        <div class="col-lg-1"></div>
                        <div class="col-lg-3">
                            <div class="well">
                               <Companyprofilecard></Companyprofilecard>
                            </div>


                        </div>

                        <div class="col-lg-7">
                            <div class="well">
                                <div class="row">
                                    <div class="col-lg-9">
                                        <h3>Basic Details</h3>
                                    </div>
                                    <div class="col-lg-1">
                                        <IconButton style={{ fontSize: 30 }} onClick={() => this.handlebasicedit()}><Edit /></IconButton>
                                    </div>
                                </div>
                                {details1}
                                {editbasicform}


                                </div>
                                <div class="well">
                                <div class="row">
                                    <div class="col-lg-9">
                                        <h3>Contact Details</h3>
                                    </div>
                                    <div class="col-lg-1">
                                        <IconButton style={{ fontSize: 30 }} onClick={() => this.handlecontactedit()}><Edit /></IconButton>
                                    </div>
                                </div>
                                {details2}
                                {editcontactform}


                                </div>



                            </div>


                        </div>
                        </div>
                        </div>

              



        );

    }
}

Companydisplayprofile.propTypes = {
    getCompanyprofile: PropTypes.func.isRequired, 
    updateCompanycontactdetails: PropTypes.func.isRequired,
    updateCompanybasicdetails: PropTypes.func.isRequired,
    profile: PropTypes.object.isRequired,

};

const mapStateToProps = state => ({
    profile: state.profile.profile,
});

export default connect(mapStateToProps, { getCompanyprofile, updateCompanybasicdetails, updateCompanycontactdetails  })(Companydisplayprofile);
