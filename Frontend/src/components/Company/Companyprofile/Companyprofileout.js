import React, { Component } from 'react';
//import '../../App.css';


import CardContent from '@material-ui/core/CardContent';

import Avatar from '@material-ui/core/Avatar';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {getCompanyprofile}  from './../../../actions/profileActions'






export class Companyprofileout extends Component {
    constructor(props) {
        super(props);
        
        }
        
        
    
    componentDidMount() {
        const { match: { params } } = this.props
        const data = {
            companyid: params.id,


        }
       console.log(data);
        this.props.getCompanyprofile(data);
       
        
}



    


    render() {
        let profiledetails = null;
        let basicdetails=null;
        let contactdetails=null;
     
        if(this.props.profile!=null)
        {
           profiledetails = (
            <CardContent style={{ textAlign: "-webkit-right", paddingTop: "10px" }} >
              
                <div style={{ textAlign: "-webkit-center" }}>
                    {this.props.profile.profilepic === null ? (
                        <Avatar className="changePhoto" title="Upload profile pic"  variant="circle" >
                         
                        </Avatar>
                    ) : (
                            <Avatar className="changePhoto" title="Change profile pic"  variant="circle" src={this.props.profile?this.props.profile.profilepic:""}  style={{ cursor: "pointer", width: "110px", height: "110px", margin: "15px", border: "0.5px solid" }} />
                        )}
                </div>
                <div style={{ textAlign: "-webkit-center" }}>
                <h2>{this.props.profile.companyname}</h2>

                </div>
               
                
            </CardContent>
        )
       
        
       
         basicdetails=
             (
                <div>


                    <br></br>
                    <h4> Name:{this.props.profile?this.props.profile.companyname:""}</h4>
                    <h4>Location: {this.props.profile?this.props.profile.companylocation:""}</h4>
                    <h4>Description:{this.props.profile?this.props.profile.companydescription:""}</h4>



                </div>
            )



        
        
    
         contactdetails =(
        
                <div>


                    <br></br>
                    <h4> Mail:{this.props.profile?this.props.profile.companymail:""}</h4>
                    <h4>Phonenumber: {this.props.profile?this.props.profile.companyphonenumber:""}</h4>



                </div>
            )



        
        
    
        return (
            <div>

                <div class="paddingleft15">


                    <div class="form-group row" paddingleft>
                        <div class="col-lg-1"></div>
                        <div class="col-lg-3">
                            <div class="well">
                                <h1>{profiledetails}</h1>
                            </div>


                        </div>

                        <div class="col-lg-7">
                            <div class="well">
                                <div class="row">
                                    <div class="col-lg-9">
                                        <h3>Basic Details</h3>
                                        {basicdetails}
                                    </div>
                                   
                                </div>
                             

                                </div>
                                <div class="well">
                                <div class="row">
                                    <div class="col-lg-9">
                                        <h3>Contact Details</h3>
                                    </div>
                                 
                                </div>
                                {contactdetails}
                            


                                </div>



                            </div>


                        </div>
                        </div>
                        </div>

              



        );

    }
}

}


Companyprofileout.propTypes = {
    
    getCompanyprofile: PropTypes.func.isRequired,
    profile: PropTypes.object.isRequired,


};

const mapStateToProps = state => ({
    profile: state.profile.profile,
});


export default connect(mapStateToProps, { getCompanyprofile })(Companyprofileout);