
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { getStudentsAppliedToJob, getResume,updateApplicationStatus } from '../../../actions/jobActions'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';



class Studentappliedtojob extends Component {
    constructor() {
        super();
        this.state = {
            studentdetails: [],
            applicationstatus: "",
            applicationid: "",
            msg: "",
            authFlag: 0,
            showresume: false,
            currentindex: 0,
            application: [],

        }

        this.handleresume = this.handleresume.bind(this);
        this.handleclose = this.handleclose.bind(this)

    }

    //get the books data from backend  


    componentWillMount() {

        const { match: { params } } = this.props
        const data = {
            jobid: params.id

        }
        console.log(data);
        this.props.getStudentsAppliedToJob(data)

    }



    applicationstatusChangeHandler = (e) => {
        this.setState({
            applicationstatus: e.target.value
        })
    }
    setAppstatus = (id) => {
        console.log(id);
        this.setState({
            applicationid: id
        })
        console.log(2);
    }

    changestatus = (event) => {
        console.log(3);
        const data = {
            applicationid: this.state.applicationid,
            applicationstatus: event.target.value,
        }

        console.log(data);
        this.props.updateApplicationStatus(data)
       
        this.setState({
            authFlag: 1,
        })




    }
    handleclose = () => {
        this.setState({
            showresume: false


        })

    }





    handleresume = (id) => {


        const data = {


            applicationid: id,

        }
      
        this.props.getResume(data)
        this.setState({
            showresume: !this.state.showresume,
            currentindex: id
        })

        console.log(this.props.application)
    }











render() {
    let displayresume = null;
    let closeresume = null;
    let details = null;

    if (this.state.showresume) {

        displayresume = (

            <div>
                <object width="550" height="600" data={this.props.application} type="application/pdf"> </object>

            </div>

        )
        closeresume = (
            <div>

                <button onClick={() => this.handleclose()} class="btn btn-danger" type="button">close Resume</button>
            </div>


        )

    }



    if (this.props.jobappliedlist.length > 0) {
        details = this.props.jobappliedlist[0].applications.map((studentdetails, index) => {
            return (<div>
                <div class="well" text-right>
                    <h5>Student name:{studentdetails.name}</h5>
                    <h5>Student mail:   {studentdetails.mail}</h5>

                    <h3>change Application status</h3>

                    <select class="form-control" id="status" onChange={this.changestatus} onClick={() => this.setAppstatus(studentdetails.applicationid)}>
                        <option selected={studentdetails.applicationstatus === "Pending" ? true : false} >Pending</option>
                        <option selected={studentdetails.applicationstatus === "Reviewed" ? true : false}>Reviewed</option>
                        <option selected={studentdetails.applicationstatus === "Declined" ? true : false}>Declined</option>
                    </select><br></br>
                    <button onClick={() => this.handleresume(studentdetails.applicationid)} class="btn btn-primary" type="button">View Resume</button>&nbsp;

                            <Link to={"/Studentprofileout/" + studentdetails.studentid} className="btn btn-primary">View Profile</Link>
                </div>
            </div>

            )
        })


    }

    return (
        <div>

            <div class="paddingleft15">


                <div class="form-group row" paddingleft>
                    <div class="col-lg-2"></div>
                    <div class="col-lg-9">  <h2>List of students applied to job</h2>





                        {details}

                        {displayresume}
                        {closeresume}



                    </div>
                </div>
            </div>
        </div>
    )
}

}
//export Home Component
Studentappliedtojob.propTypes = {

    getStudentsAppliedToJob: PropTypes.func.isRequired,
    getResume: PropTypes.func.isRequired,

};

const mapStateToProps = state => ({
    jobappliedlist: state.studentlist.jobappliedlist,
    application: state.studentlist.resume
});

export default connect(mapStateToProps, { getStudentsAppliedToJob, getResume,updateApplicationStatus })(Studentappliedtojob);
