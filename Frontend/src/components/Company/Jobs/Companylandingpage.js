import React,{ Component } from "react";
import { Link } from 'react-router-dom';
import dateFormat from 'dateformat';
import {getCompanyJoblist} from '../../../actions/jobActions'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TablePagination from '@material-ui/core/TablePagination';



class Companylandingpage extends Component
{
    constructor()
    {
      super();
        this.state = 
    {
        companyjoblist :[],
        page:0,
        rowsPerPage:5,
        
       
    }
}
    
    //this.handleChangePage=this.handleChangePage.bind(this);
    //this.handleChangeRowsperPage=this.handleChangeRowsperPage.bind(this);
 

 componentDidMount(){

    const data = { companyid : sessionStorage.getItem('companyid')  }
    console.log(data);
    this.props.getCompanyJoblist(data);
      
}

 
  // emptyRows = this.state.rowsPerPage - Math.min(this.state.rowsPerPage, this.pro this.state.rowsPerPage);

  
  handleChangePage = (event, newPage) => {
    this.setState({
        page: newPage
    })
   };
   handleChangeRowsPerPage = (event) => {
    this.setState({ 
        rowsPerPage: (parseInt(event.target.value, 10)),
        page: 0,
     })
    

  };

render() {
  
    let displayform=null;
    console.log(this.props.companyjoblist.length);
    if(this.props.companyjoblist.length>0)
        {
        
        displayform =  (  


           
           (this.state.rowsPerPage > 0
                ?  this.props.companyjoblist.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage)
                : this.props.companyjoblist
              ).map((jobdetails,index) => {
          return(
            
            
                <div>
                    <div class="well" text-right>
                         <h5>job id:{jobdetails.jobid}</h5>
                        <h5>job Title:   {jobdetails.jobtitle}</h5>
                        <h5>job posting date:  {dateFormat(jobdetails.jobpostingdate, "mmmm dS, yyyy")}</h5>
                        <h5>job application deadline:  {dateFormat(jobdetails.jobapplicationdeadline, "mmmm dS, yyyy")}</h5>
                        <h5>job location :  {jobdetails.joblocation}</h5>
                        <h5>job salary:  {jobdetails.jobsalary}</h5>
                        <h5>job Description:  {jobdetails.jobdescription}</h5>
                        <h5>job Category:  {jobdetails.jobcategory}</h5>
                        <Link to={"/Studentappliedtojob/"+jobdetails.jobid}   className="btn btn-primary">Students Applied</Link>
                        
                    </div>
                    </div>
             
          )
      }))
        }
      
      

      return (
             <div>
    
                <div class="paddingleft15">
                    <div class="form-group row" paddingleft>

                    <div class="col-lg-10"> </div>
                   
                    <div class="col-lg-1"><Link to="/Addjobpage" className="btn btn-primary">Add job</Link> </div>
                    </div>
                      
                    <div class="form-group row" paddingleft>
                        <div class="col-lg-2"></div>
                        <div class="col-lg-9">  <h2>List of jobs posted by company</h2>
                    
       
                        
                        
                    
                        {displayform}
                   

                        <h1><TablePagination 
                            style={{ fontSize: 10 }}
                          rowsPerPageOptions={[5, 10, 25]}
                          component="div"
                          count={this.props.companyjoblist.length}
                          rowsPerPage={this.state.rowsPerPage}
                          page={this.state.page}
                          onChangePage={this.handleChangePage}
                          onChangeRowsPerPage={this.handleChangeRowsPerPage}/></h1>
                    
            
                        </div>
                        
                        </div>
                        </div>

            
            </div>
          
           
           
           
        )
    }

}

Companylandingpage.propTypes = {
    
    getCompanyJoblist: PropTypes.func.isRequired,

   
};

const mapStateToProps = state => ({
    companyjoblist: state.companyjoblist.companyjoblist,
});

export default connect(mapStateToProps, { getCompanyJoblist  })(Companylandingpage);
