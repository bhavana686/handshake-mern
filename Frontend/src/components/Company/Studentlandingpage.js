import React, { Component } from 'react';

import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getAllstudents } from '../../actions/studentActions';
import TablePagination from '@material-ui/core/TablePagination';




class Studentlandingpage extends Component {
    constructor() {
        super();
        this.state = {
           
            studentSearch: "",
            filterlist: [],
            studentlist: [],
            page:0,
            rowsPerPage:5,
            
            

        }
        this.studentSearchHandler = this.studentSearchHandler.bind(this);
                this.applyfilter=this.applyfilter.bind(this);
        


    }
  
    //get the books data from backend  
    componentWillReceiveProps(nextProps) {
        if (nextProps.studentlist) {
            var { studentlist } = nextProps;

            this.setState({
                  filterlist:studentlist

            });
        }
    }
    componentDidMount() {

        this.props.getAllstudents();
       
        console.log(this.state.filterlist);
      
             
           
    }
    studentSearchHandler(e) {
        this.applyfilter(e.target.value.toLowerCase())

        this.setState({
            studentSearch: e.target.value
        });
        
    }
   
    applyfilter = (p1) =>
     {
        var resultlist = [];
        for (var i = 0; i < this.props.studentlist.length; i++) {

            if (this.props.studentlist[i].name.toLowerCase().includes(p1) || this.props.studentlist[i].collegename.toLowerCase().includes(p1))
            {
                resultlist.push(this.props.studentlist[i])
            }
            if(this.props.studentlist[i].skillset!=null && this.props.studentlist[i].skillset!=="" && this.props.studentlist[i].skillset.toLowerCase().includes(p1))
            {
                resultlist.push(this.props.studentlist[i])

            }

        }
        this.setState({
            filterlist:resultlist

        })

    }
    handleChangePage = (event, newPage) => {
        this.setState({
            page: newPage
        })
       };
       handleChangeRowsPerPage = (event) => {
        this.setState({ 
            rowsPerPage: (parseInt(event.target.value, 10)),
            page: 0,
         })
        
    
      };
    

   
        

    render() {
        
        
       let details=null;
        console.log(this.props.studentlist);
        console.log(this.state.filterlist);
        
          if(this.state.filterlist.length>0 && this.state.filterlist!=="")
          {
        //iterate over books to create a table row
           details =(
            (this.state.rowsPerPage > 0
                ?  this.state.filterlist.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage)
                : this.state.filterlist
              ).map((studentlist,index)=> {
           
            return (
                <div class="well" text-right>
               <div class="form-group row">
                    <div class="col-md-8">
            
                    <h5> Name:  {studentlist.name}</h5>
                    <h5> Mail :  {studentlist.mail}</h5>
                    <h5> Collegename :  {studentlist.collegename}</h5>
                    <h5> Skills :  {studentlist.skillset}</h5>
                    <Link to={"/Studentprofileout/"+studentlist.studentid} 
                      className="btn btn-primary">View Profile</Link>      
                    </div>
                    <div class="col-md-3"><br></br><br></br><br></br> 
                    <Link to={{ pathname: "/Messages",
                                       state: {
                                             studentlist:studentlist
                                            } 
                        }}   className="btn btn-primary">Message</Link>
                               
                     
                    </div>
                </div>
                </div>


            )
        }))
    }
        return (
            <div class="paddingleft15">


                <div class="form-group row">
                    <div class="col-md-3"></div>
                    <div class="col-md-7">
                     
                        <div class="well" text-right>
                            
                           
                            <input type="text"  onChange={this.studentSearchHandler} class="form-control" name="studentsearch"
                                placeholder=" Search by student name,collegename,skillset" required /><br></br>
                                <div>{details}</div>
                                <h1><TablePagination 
                            style={{ fontSize: 10 }}
                          rowsPerPageOptions={[5, 10, 25]}
                          component="div"
                          count={this.state.filterlist.length}
                          rowsPerPage={this.state.rowsPerPage}
                          page={this.state.page}
                          onChangePage={this.handleChangePage}
                          onChangeRowsPerPage={this.handleChangeRowsPerPage}/></h1>
                    
                           
                            



                        </div>
                    </div>
                    <div class="col-md-2">
                
                    </div>

                </div>



            </div>

        )

    }
}
//export Home Component
Studentlandingpage.propTypes = {
    
    getAllstudents: PropTypes.func.isRequired,

};

const mapStateToProps = state => ({
    studentlist: state.studentlist.studentlist,
});

export default connect(mapStateToProps, { getAllstudents  })(Studentlandingpage);
