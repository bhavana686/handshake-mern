import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { companySignup } from '../actions/signupActions'
import { Redirect } from 'react-router';
import { Link } from 'react-router-dom';





class CompanySignup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            signupFlag:0
            
        };
        
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.user) {
            var { user } = nextProps;

            if (user === "COMPANY_ADDED" || user === "COMPANY_EXISTS") {
                this.setState({
                    signupFlag: 1
                });
            }
        }
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }


    onSubmit = (e) => {
        //prevent page from refresh
        e.preventDefault();
       
            const data = {
           
                logic:"companysignup",
                companyname : this.state.companyname,
                companymail : this.state.companymail,
                password: this.state.password,
                companylocation : this.state.companylocation
            }
            console.log(data)

        this.props.companySignup(data);


    
}

               
            
    render() {
        //redirect based on successful signup
        let redirectVar = null;
        let message = "";
       
      
        if (this.props.user === "COMPANY_ADDED" && this.state.signupFlag) {
            alert("You have registered successfully");
            redirectVar = <Redirect to="/CompanyLogin" />
        }
        if(this.props.user === "COMPANY_EXISTS" && this.state.signupFlag){
            message = "Email id is already registered"
        }
        
        
        
        return (
            <div>

             {redirectVar}
           
    <div class="container">
                <div class="row">
                <div class="col-lg-10 col-xl-9 mx-auto">
                <div class="card card-signin flex-row my-5">
                
                <div class="card-body">
                <h5 class="card-title text-center"> COMPANY SIGN IN</h5>
                <form onSubmit={this.onSubmit}>
                                       
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="companymail" onChange={this.onChange} placeholder="Email Id" pattern="^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$'%&*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])$" title="Please enter valid email address" required />
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" name="password" onChange={this.onChange} placeholder="Password" required />
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="companyname" onChange={this.onChange} placeholder="company name" required />
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="companylocation" onChange={this.onChange} placeholder="company location" required />
                                        </div>
                                        <div style={{ color: "#ff0000" }}>{message}</div><br />
                                        <button type="submit" class="btn btn-primary">Signup</button><br /><br/>
                                        <div>Already have an account? <Link to='/CompanyLogin'>Login</Link></div>
                                        
                                    </form>
                
                </div>
                </div> 
                </div>
                </div> 
                </div>  
                </div>
        )
       

    }
}

CompanySignup.propTypes = {
    companySignup: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired
};
const mapStateToProps = state => {
   
        return ({
            user: state.signup.user
        })
  
    
  };



export default connect(mapStateToProps, { companySignup })(CompanySignup);