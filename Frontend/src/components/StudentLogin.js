
import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { studentLogin } from '../actions/signupActions';
const jwt_decode = require('jwt-decode');

class StudentLogin extends Component {
    //call the constructor method
    constructor(props) {
        //Call the constrictor of Super class i.e The Component
        super(props);
        //maintain the state required for this component
        this.state = {};
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    //submit Login handler to send a request to the node backend
    onSubmit = (e) => {
        e.preventDefault();
        const data = {
            mail: this.state.mail,
            password: this.state.password,
            logic:"studentlogin",
        }

        this.props.studentLogin(data);

        this.setState({
            loginFlag: 1
        });
    }

    render() {
        let redirectVar = null;
        let message = ""

        if (this.props.user && this.props.user.token) {
            sessionStorage.setItem("token", this.props.user.token);

            var decoded = jwt_decode(this.props.user.token.split(' ')[1]);
            sessionStorage.setItem("studentid", decoded.studentid);
            sessionStorage.setItem("mail", decoded.mail);
            sessionStorage.setItem("name", decoded.name);

            sessionStorage.setItem("loggintype","student");
            redirectVar = <Redirect to="/Studentpage" />
        }
        else if(this.props.user === "STUDENT_NOTEXISTS" && this.state.loginFlag){
            message = "No user with this email id";
        }
        else if(this.props.user === "INCORRECT_PASSWORD" && this.state.loginFlag){
            message = "Incorrect Password";
        }
  
  
        return(
            <div>

             {redirectVar}
           
    <div class="container">
                <div class="row">
                <div class="col-lg-10 col-xl-9 mx-auto">
                <div class="card card-signin flex-row my-5">
                
                <div class="card-body">
                <h5 class="card-title text-center"> STUDENT LOG IN</h5>
                <form onSubmit={this.onSubmit}>
                                       
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="mail" onChange={this.onChange} placeholder="Email Id" pattern="^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$'%&*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])$" title="Please enter valid email address" required />
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" name="password" onChange={this.onChange} placeholder="Password" required />
                                        </div>
                                       
                                        <div style={{ color: "#ff0000" }}>{message}</div><br />
                                        <button type="submit" class="btn btn-primary">Log in</button><br /><br/>
                                        <div>Already have an account? <Link to='/StudentSignup'>Signin</Link></div>
                                        
                                    </form>
                
                </div>
                </div> 
                </div>
                </div> 
                </div>  
                </div>
        )
    }
}

StudentLogin.propTypes = {
    studentLogin: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired
}

const mapStateToProps = state => { 
    if(state.login){
    return ({
        user: state.login.user
    })
}
else{
    return ({
        user: state.user
    })
}

};

export default connect(mapStateToProps, { studentLogin })(StudentLogin);