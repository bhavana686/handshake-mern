import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { studentSignup } from '../actions/signupActions'
import { Redirect } from 'react-router';
import { Link } from 'react-router-dom';







class StudentSignup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };
        
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.user) {
            var { user } = nextProps;

            if (user === "STUDENT_ADDED" || user === "STUDENT_EXISTS") {
                this.setState({
                    signupFlag: 1
                });
            }
        }
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }


    onSubmit = (e) => {
        //prevent page from refresh
        e.preventDefault();
       
            const data = {
            
                logic:"studentsignup",
                name : this.state.name,
                mail : this.state.mail,
                password: this.state.password,
                collegename : this.state.collegename
            }

        this.props.studentSignup(data);


    
}              
        
   render() {
        //redirect based on successful signup
        let redirectVar = null;
        let message = "";
       
      
        if (this.props.user === "STUDENT_ADDED" && this.state.signupFlag) {
            alert("You have registered successfully");
            redirectVar = <Redirect to="/StudentLogin" />
        }
        if(this.props.user === "STUDENT_EXISTS" && this.state.signupFlag){
            message = "Email id is already registered"
        }
        
        return (
            <div>

             {redirectVar}
           
    <div class="container">
                <div class="row">
                <div class="col-lg-10 col-xl-9 mx-auto">
                <div class="card card-signin flex-row my-5">
                
                <div class="card-body">
                <h5 class="card-title text-center"> STUDENT SIGN IN</h5>
                <form onSubmit={this.onSubmit}>
                                       
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="mail" onChange={this.onChange} placeholder="Email Id" pattern="^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$'%&*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])$" title="Please enter valid email address" required />
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" name="password" onChange={this.onChange} placeholder="Password" required />
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" onChange={this.onChange} placeholder="Student name" required />
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="collegename" onChange={this.onChange} placeholder="College Name" required />
                                        </div>
                                        <div style={{ color: "#ff0000" }}>{message}</div><br />
                                        <button type="submit" class="btn btn-primary">Signup</button><br /><br/>
                                        <div>Already have an account? <Link to='/Studentlogin'>Login</Link></div>
                                        
                                    </form>
                
                </div>
                </div> 
                </div>
                </div> 
                </div>  
                </div>
        )
       

    }
}

StudentSignup.propTypes = {
    studentSignup: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired
};
const mapStateToProps = state => {
   
        return ({
            user: state.signup.user
        })
  
    
  };



export default connect(mapStateToProps, { studentSignup })(StudentSignup);