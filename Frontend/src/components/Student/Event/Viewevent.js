/* eslint-disable jsx-a11y/anchor-is-valid */

import React, { Component } from 'react';
//import '../../App.css';
import axios from 'axios';
import { Link } from 'react-router-dom';
import dateFormat from 'dateformat';
import {rooturl} from '../../../webConfig';



class Viewevent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Eventdetails:[],
            major:"",
            displaytext:"Register" ,
            status:"",
           
        }
        
        this.registersubmit=this.registersubmit.bind(this);
        //this.getstatus=this.getstatus.bind(this);
      
        
    }
 
    
 
    componentDidMount() {
        //this.getstatus();

        const { match: { params } } = this.props
        const data = {
            studentid:sessionStorage.getItem('studentid'),
            eventid: params.id

        }
        console.log(data.eventid)
        console.log(data);
        axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
        axios.post(`${rooturl}/student/studenteventdetails`,data)
            .then((response) => {
                console.log(response)
                console.log(response.data[0])
              if(response.status===200)
              {
                this.setState({
                    Eventdetails:response.data[0],
                  
                });
            }
            });
            axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
            axios.post(`${rooturl}/student/getmajor`,data)
            .then((response) => {
                console.log(response)
                console.log(response.data)
              if(response.status===200)
              {
                this.setState({
                     major  :response.data,
                  
                });
            }
            });
            console.log(this.state.Eventdetails)
        
    }
// getstatus()
//     {
//         const { match: { params } } = this.props
//         const data = {
//             studentid: sessionStorage.getItem('studentid'),
//             eventid:params.id
            
//         }
           
//         console.log(data);
//         //set the with credentials to true
//         axios.defaults.withCredentials = true;
//         //make a post request with the user data
//         axios.post(rooturl+'/event/getstatus', data)
//             .then(response => {
//                 console.log("Status Code : ", response.status);
//                 if (response.status === 200) 
//                 {
//                     this.setState({
//                         status:response.data
   

//                     })

//                 } 
    
//             });

//     }
    registersubmit = (e) => {
        const { match: { params } } = this.props
        const data = {
            studentid: sessionStorage.getItem('studentid'),
            eventid:params.id,
            eventstatus: "Applied",
            name:sessionStorage.getItem('name'),
            mail:sessionStorage.getItem('mail'),

        }
           
        console.log(data);
        //set the with credentials to true
        axios.defaults.withCredentials = true;
        //make a post request with the user data
        axios.defaults.headers.common['authorization'] = sessionStorage.getItem('token');
        axios.post(`${rooturl}/event/registerforevent`,data)
            .then(response => {
                console.log("Status Code : ", response.status);
                if (response.status === 200) {
                    this.setState({
                        displaytext:"Registered"
                    })
                   // this.getstatus()
                } 
                else{
                    this.setState({
                        displaytext:"Register"
   

                    })
                }
            });
    }
    render() {
        
        let showbutton=null;
        if(this.state.major!=null)
        {
        //    if(this.state.status=="Applied") 
        //    {
        //      showbutton=(
        //         <div>
        //              <button type="button" class="btn btn-success">Already Registered</button>
 
        //         </div>
        //     )

        //    }
        
         if(this.state.major===this.state.Eventdetails.eventeligibility || (this.state.Eventdetails.eventeligibility==="all" && this.state.status!=="Applied"))
           
        {
            showbutton=(
                <div>
                     <button type="button" onClick={()=>this.registersubmit()}class="btn btn-primary">{this.state.displaytext}</button>
 
                </div>
            )
        }
        else 
        {
            showbutton=(
                <div>


 
                       <button type="button" class="btn btn-primary disabled">Register</button>


                </div>
            )

        }
    
    }
    else{
        showbutton=(
            <div>


                <h5>add education details to check eligibility</h5>


            </div>
        )

    }
            

        //iterate over books to create a table row
        let details =  
             (<div>
                <div class="well" text-right>
                    <h5>name:{this.state.Eventdetails.eventname}</h5>
                    <h5>Eventid:{this.state.Eventdetails.eventid}</h5>

                    <h5>description:{this.state.Eventdetails.eventdescription}</h5>
                    <h5> time:{this.state.Eventdetails.eventtime}</h5>
                    <h5> date:{dateFormat(this.state.Eventdetails.eventdate, "mmmm dS, yyyy")}</h5>
                    <h5> location:{this.state.Eventdetails.eventlocation}</h5> 
                    <h5> eligibility:{this.state.Eventdetails.eventeligibility}</h5>
                   


                    {showbutton}




                   
                        
            </div>
            </div>

            )
        
        return (
            <div>
            <nav class="navbar">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand">Upcoming Events</a>
                        </div>
                        <ul class="nav navbar-nav navbar-right">


                            <li><Link to="/Studenteventpage">Upcoming Events</Link></li>
                            <li><Link to="/Eventsearch">Event Search</Link></li>
                            <li><Link to="/Registerevent">Registered Events</Link></li>



                        </ul>
                    </div>
                </nav>
                
                <div class="paddingleft15">

                    
                      
                    <div class="form-group row" paddingleft>
                        <div class="col-lg-2"></div>
                        <div class="col-lg-9">  <h2>Event Details</h2>
                        
                

                            

                            {details}

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
//export Home Component
export default Viewevent;