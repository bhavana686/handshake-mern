/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { Link } from 'react-router-dom';
import TablePagination from '@material-ui/core/TablePagination';
import { getEventsnotregistered} from '../../../actions/eventActions';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';





class Studenteventpage extends Component {
    constructor(props) {

        super(props);
        this.state =
        {
            Eventdetails: [],
            page:0,
             rowsPerPage:5,

        }
    }

    componentDidMount() { 
        const data = {
            studentid: sessionStorage.getItem('studentid'),
        }
        this.props.getEventsnotregistered(data)    

    }
    handleChangePage = (event, newPage) => {
        this.setState({
            page: newPage
        })
       };
       handleChangeRowsPerPage = (event) => {
        this.setState({ 
            rowsPerPage: (parseInt(event.target.value, 10)),
            page: 0,
         })
        
    
      };

    render() {
        var button = null;;
        var displayform = null;

        displayform =  (   
            (this.state.rowsPerPage > 0
                ?  this.props.eventsnotreglist.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage)
                : this.props.eventsnotreglist
              ).map((Eventdetails,index)=> {
        
          
          return(
                        <div>

                        <div class="paddingleft15">
                            <div class="form-group row" paddingleft>
                                <div class="col-lg-3">        </div>
                                <div class="col-lg-4">
                                    <div class="well" text-right>
                                        <h5>Event Title:   {Eventdetails.eventname}</h5>
                                        
                                        <Link to={"/Viewevent/"+Eventdetails.eventid} Eventdetails={this.state.Eventdetails} className="btn btn-primary">View Details</Link>
                     

                                    </div>
                                </div>
                            </div>
                        </div>
                     </div>
                    )
                }))

        

        return (
            <div>
                <nav class="navbar">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand">Upcoming Events</a>
                        </div>
                        <ul class="nav navbar-nav navbar-right">


                            <li><Link to="/Studenteventpage">Upcoming Events</Link></li>
                            <li><Link to="/Eventsearch">Event Search</Link></li>
                            <li><Link to="/Registerevent">Registered Events</Link></li>



                        </ul>
                    </div>
                </nav>
                <div class="paddingleft15">
                    <div class="form-group row" paddingleft>
                        <div class="col-lg-3"></div>
                        <div class="col-lg-3"></div>
                        <div class="col-lg-3">  {button}  </div>
                    </div>
                </div>

                {displayform}
                <h1><TablePagination
                                        style={{ fontSize: 10 }}
                                        rowsPerPageOptions={[5, 10, 25]}
                                        component="div"
                                        count={this.state.Eventdetails.length}
                                        rowsPerPage={this.state.rowsPerPage}
                                        page={this.state.page}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage} /></h1>

              


            </div>


        );
    }

}

Studenteventpage.propTypes = {
    getEventsnotregistered : PropTypes.func.isRequired,
    eventsnotreglist: PropTypes.array.isRequired,

};

const mapStateToProps = state => ({
    eventsnotreglist: state.studentlist.eventsnotreglist,
});

export default connect(mapStateToProps, { getEventsnotregistered })(Studenteventpage);