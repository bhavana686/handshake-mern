/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import TablePagination from '@material-ui/core/TablePagination';
import { getAllevents } from '../../../actions/eventActions';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';


class Eventsearch extends Component {
    constructor() {
        super();
        this.state = {
            alleventslist: [],
            search: "",
            page:0,
            rowsPerPage:5,
            

        }


    }
    //get the books data from backend  
    componentDidMount() {
        this.props.getAllevents()    
            
    }
    handleChangePage = (event, newPage) => {
        this.setState({
            page: newPage
        })
       };
       handleChangeRowsPerPage = (event) => {
        this.setState({ 
            rowsPerPage: (parseInt(event.target.value, 10)),
            page: 0,
         })
        
    
      };
    onChange = e => {
        this.setState(
            {
                search: e.target.value
            }
        );
    }
  

    render() {
        const { search } = this.state;
       
        // var sname=this.state.studentlist.name.toLowerCase()
        if ({ search } !== "") {
            var filteredStudents = this.props.alleventslist.filter(eventlist => {
                return eventlist.eventname.indexOf(search) !== -1
            });
        }
       

        //iterate over books to create a table row
        let details =    (this.state.rowsPerPage > 0
            ?  filteredStudents.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage)
            : filteredStudents
          ).map((eventlist ,index)=> {
            return (


                <div class="well" text-right>
                    <h5>Event Name:   {eventlist.eventname}</h5>
                    <h5>Event Description :  {eventlist.eventdescription}</h5>
                    <h5>Event Time  :  {eventlist.eventtime}</h5>
                    <h5>Event Date :  {eventlist.eventdate}</h5>
                    <h5>Event Location :  {eventlist.eventlocation}</h5>
                    <h5>Event Eligibility  :  {eventlist.eventeligibility}</h5>
                   

                </div>


            )
        })
        //if not logged in go to login page
       
        return (
            <div>
            <nav class="navbar">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand">Search Events</a>
                        </div>
                        <ul class="nav navbar-nav navbar-right">

                           
                            <li><Link to="/Studenteventpage">Upcoming Events</Link></li>
                            <li><Link to="/Eventsearch">Event Search</Link></li>
                            <li><Link to="/Registerevent">Registered Events</Link></li>
    
                            
                           
                        </ul>
                    </div>
                </nav>
            <div class="paddingleft15">


                <div class="form-group row">
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <div class="well" text-right>
                        <h2>Filters</h2><br></br>
                            <h3>Search</h3><br></br>
                            <input type="text" onChange={this.onChange} class="form-control" name="eventsearch"
                                placeholder=" Search by event name" required /><br></br>
                            



                        </div>
                    </div>
                    <div class="col-md-6">
                        {details}
                        <h1><TablePagination
                                        style={{ fontSize: 10 }}
                                        rowsPerPageOptions={[5, 10, 25]}
                                        component="div"
                                        count={filteredStudents.length}
                                        rowsPerPage={this.state.rowsPerPage}
                                        page={this.state.page}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage} /></h1>

                    </div>

                </div>



            </div>
            </div>

        )

    }
}
//export Home Component

Eventsearch.propTypes = {
    getAllevents : PropTypes.func.isRequired,
    

    alleventlist: PropTypes.array.isRequired,

};

const mapStateToProps = state => ({
    alleventslist: state.studentlist.alleventslist,
});

export default connect(mapStateToProps, { getAllevents })(Eventsearch);