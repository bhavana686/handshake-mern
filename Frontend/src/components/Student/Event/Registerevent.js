/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";


import { Link } from 'react-router-dom';

import dateFormat from 'dateformat';



import TablePagination from '@material-ui/core/TablePagination';
import { geteventsregistered } from '../../../actions/eventActions';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';







class Registerevent extends Component {
    constructor(props) {

        super(props);
        this.state =
        {
            Eventdetails: [],
            page:0,
            rowsPerPage:5,
          
        }
    }


    componentDidMount() {

        const data = {
            studentid: sessionStorage.getItem('studentid'),


        }
        this.props.geteventsregistered(data)   
    }
    handleChangePage = (event, newPage) => {
        this.setState({
            page: newPage
        })
       };
       handleChangeRowsPerPage = (event) => {
        this.setState({ 
            rowsPerPage: (parseInt(event.target.value, 10)),
            page: 0,
         })
        
    
      };
    render() {
       
        var displayform = null;

        displayform = (

            (this.state.rowsPerPage > 0
                ?  this.props.eventsreglist.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage)
                : this.props.eventsreglist
              ).map((Eventdetails,index)=> {
        
          
                return (
                    <div>

                    <div class="paddingleft15">
                        <div class="form-group row" paddingleft>
                            <div class="col-lg-3">        </div>
                            <div class="col-lg-4">
                                <div class="well" text-right>
                                    <h3>{Eventdetails.eventname}</h3> 
                                   <h5>description:{Eventdetails.eventdescription}</h5>
                                     <h5> time:{Eventdetails.eventtime}</h5>
                                     <h5> date:{dateFormat(Eventdetails.eventdate, "mmmm dS, yyyy")}</h5>
                                  <h5> location:{Eventdetails.eventlocation}</h5> 
                                    <h5> eligibility:{Eventdetails.eventeligibility}</h5>


                                    
                                    


                                  

                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
                )
            }))


        
        return (
            <div>
                <nav class="navbar">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand">Registered Events</a>
                        </div>
                        <ul class="nav navbar-nav navbar-right">


                            <li><Link to="/Studenteventpage">Upcoming Events</Link></li>
                            <li><Link to="/Eventsearch">Event Search</Link></li>
                            <li><Link to="/Registerevent">Registered Events</Link></li>



                        </ul>
                    </div>
                </nav>
                <div class="paddingleft15">
                    <div class="form-group row" paddingleft>
                        <div class="col-lg-3"></div>
                        
                    </div>
                </div>

                {displayform}

                <h1><TablePagination
                                        style={{ fontSize: 10 }}
                                        rowsPerPageOptions={[5, 10, 25]}
                                        component="div"
                                        count={this.state.Eventdetails.length}
                                        rowsPerPage={this.state.rowsPerPage}
                                        page={this.state.page}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage} /></h1>

               


            </div>
     


        );
    }

}


Registerevent.propTypes = {
    getEventsnotregistered : PropTypes.func.isRequired,
    eventsreglist: PropTypes.array.isRequired,

};

const mapStateToProps = state => ({
    eventsreglist: state.studentlist.eventsreglist,
});

export default connect(mapStateToProps, { geteventsregistered })(Registerevent);