import React, { Component } from 'react';
//import '../../App.css';

import Experience2 from './Experience2';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getStudentprofile, addExperience  } from '../../../actions/profileActions';







export class Experience1 extends Component {
    constructor() {
        super();
        this.state = {
            profile:{},
            experiencerecord:[],
        
            authFlag: 0,
            add: false,
            companyname: "",
            experiencetitle: "",
            experiencelocation: "",
            experiencestartdate: "",
            experienceenddate: "",
            experienceworkdescription: ""

        }
        this.handleadd = this.handleadd.bind(this); 
        this.onChange = this.onChange.bind(this);

    }
   
    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    componentDidMount() {
     
        const data ={
            studentid:sessionStorage.getItem('studentid')

        };
        this.props.getStudentprofile(data);


    }
    submitExperience = (e) => {
     
        e.preventDefault();

        const data = {
            studentid: sessionStorage.getItem('studentid'),
            
            companyname: this.state.companyname,
            experiencetitle:this.state.experiencetitle,
            experiencelocation: this.state.experiencelocation,
            experiencestartdate: this.state.experiencestartdate,
            experienceenddate: this.state.experienceenddate,
            experienceworkdescription: this.state.experienceworkdescription,
      
        }
        console.log(data);
        this.props.addExperience(data);
        //set the with credentials to true
        console.log(this.props.profile)
        
        //this.props.getStudentprofile(data);

        this.setState({
            
            add:false,

        })
      

    }

    handleadd = () => {
        this.setState({
            add: !this.state.add

        })
    }

    changeHandler = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }


 
    render() {
        let details = null;

        let addform = (<div><button onClick={this.handleadd}>
            Add Experience
          </button>

        </div>)
          console.log(this.props.profile)
        if (this.state.add) {

                addform =

                    (
                        <div>
                            <form onSubmit={this.submitPost}>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-10 col-xl-9 mx-auto">
                                            <div class="card card-signin flex-row my-5">
                                            <div class="form-label-group">
                                        <label class="control-label col-sm-2" for="companyname">Company Name:</label>
                                        <div class="col-sm-3">
                                            <input type="text" name="companyname" id="companyname" onChange={this.onChange} class="form-control" required />

                                        </div><br /><br />
                                        <div class="form-label-group">
                                            <label class="control-label col-sm-2" for="experiencetitle">Title:</label>
                                            <div class="col-sm-3">
                                                <input type="text" name="experiencetitle" id="experiencetitle" onChange={this.onChange} class="form-control" required />
                                            </div>
                                        </div><br /><br />
                                        <div class="form-label-group">
                                            <label class="control-label col-sm-2" for="experiencelocation">Location:</label>
                                            <div class="col-sm-3">


                                                <input type="text" name="experiencelocation" id="experiencelocation" onChange={this.onChange} class="form-control" required />
                                            </div>
                                        </div><br /><br />
                                        <div class="form-label-group">
                                            <label class="control-label col-sm-2" for="experiencestartdate">Start Date:</label>
                                            <div class="col-sm-3">
                                                <input type="date" name="experiencestartdate" id="experiencestartdate" onChange={this.onChange} class="form-control" required />
                                            </div>
                                        </div><br /><br />
                                        <div class="form-label-group">
                                            <label class="control-label col-sm-2" for="experienceenddate">End Date</label>
                                            <div class="col-sm-3">
                                                <input type="date" name="experienceenddate" id="experienceenddate" onChange={this.onChange} class="form-control" required />
                                            </div>
                                        </div><br /><br />
                                        <div class="form-label-group">
                                            <label class="control-label col-sm-2" for="experienceworkdescription">Work Description:</label>
                                            <div class="col-sm-3">
                                                <input type="text" name="experienceworkdescription" id="experienceworkdescription" onChange={this.onChange} class="form-control" required />
                                            </div><br /><br />
                                        </div>
                                        <br /><br />
                                        <div class="form-group">
                                            <button onClick={this.submitExperience} class="btn btn-success" type="submit">Add</button>&nbsp;
                                            <button onClick={this.handleadd} class="btn btn-success" type="submit">Cancel</button>&nbsp;
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>


                    );
            }
            
            else{
                console.log(this.props.profile)
                if (this.props.profile.experience!=null) {
                    //if(this.props.profile[0]!=null)
                   // {
                         details= this.props.profile.experience.map((experience,index) => {
                            return (
                                   <Experience2 experiencerecord={experience}></Experience2>
                                  
                                
                          )
                     
                            
                        
                            })
                    
                }

              
        
            }



            return (
                <div>
                    <h3>Experience </h3>

                    {details}

                    {addform}






                </div>





            );
        }
    }



Experience1.propTypes = {
        getStudentprofile: PropTypes.func.isRequired,
        addExperience: PropTypes.func.isRequired,
        profile: PropTypes.object.isRequired,
    
    };
    
    const mapStateToProps = state => ({
        profile: state.profile.profile,
    });
    
    export default connect(mapStateToProps, { getStudentprofile, addExperience })(Experience1);
    