import React, { Component } from 'react';
//import '../../App.css';

import IconButton from '@material-ui/core/IconButton';
import { Edit } from '@material-ui/icons';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getStudentprofile, updateEducation , deleteEducation} from '../../../actions/profileActions';
 
 
 
 
export class Education2 extends Component {
   constructor(props) {
       super(props);
       this.state = {
           educationrecord: [],
           authFlag: 0,
 
           edit: false,
           collegename:"",
           educationlocation:"",
           educationdegree:"",
           educationmajor:"",
           educationyearofpassing:"",
           educationcurrentcgpa:""
       }
 
     
       this.onChange = this.onChange.bind(this);
       this.handleedit = this.handleedit.bind(this);
       this.changeHandler = this.changeHandler.bind(this);
       this.submitDelete=this.submitDelete.bind(this)
 
 
   }
 
 
   componentDidMount() {
        //  const data ={
        //     studentid:sessionStorage.getItem('studentid')

        // };
        // this.props.getStudentprofile(data);

     
      this.setState({
           collegename:this.props.educationrecord.collegename,
           educationlocation:this.props.educationrecord.educationlocation,
           educationdegree: this.props.educationrecord.educationdegree,
           educationmajor:this.props.educationrecord.educationmajor,
           educationyearofpassing:this.props.educationrecord.educationyearofpassing,
           educationcurrentcgpa:this.props.educationrecord.educationcurrentcgpa
 
       });
 
   }
   componentWillReceiveProps(nextProps) {
    if (nextProps.educationrecord) {
        var { educationrecord } = nextProps;

        var userData = {
           
          
           
            collegename:educationrecord.collegename|| this.state.collegename,
            educationlocation: educationrecord.educationlocation ||this.state.educationlocation,
            educationdegree: educationrecord.educationdegree||this.state.educationdegree,
            educationmajor: educationrecord.educationmajor||this.state.educationmajor,
             educationyearofpassing: educationrecord.educationyearofpassing||this.state.educationyearofpassing,
            educationcurrentcgpa: educationrecord.educationcurrentcgpa||this.state.educationcurrentcgpa,
  

        };

        this.setState(userData);
    }
}
 
 
   onChange = (e) => {
       this.setState({
           [e.target.name]: e.target.value
       })
   }
  
   submitEdit = (e) => {
 
      
       e.preventDefault();
 
       const data = {
           studentid: sessionStorage.getItem('studentid'),
           educationdetailsid:this.props.educationrecord.educationdetailsid,
           collegename: this.state.collegename,
           educationlocation: this.state.educationlocation,
           educationdegree: this.state.educationdegree,
           educationmajor: this.state.educationmajor,
           educationyearofpassing: this.state.educationyearofpassing,
           educationcurrentcgpa: this.state.educationcurrentcgpa,
 
 
       }
       this.props.updateEducation(data);
      this.setState({
         
           edit:false
       })
      
 
   }
   submitDelete = () => {
     
 
       const data = {
           studentid: sessionStorage.getItem('studentid'),
           educationdetailsid:this.props.educationrecord.educationdetailsid,
       }
       this.props.deleteEducation(data);
       this.setState({
         
           edit:false
       })
      
 
   }
   
  
   handleedit = () => {
       this.setState({
           edit: !this.state.edit
 
       })
   }
   changeHandler = (e) => {
       this.setState({
           [e.target.name]: e.target.value
       })
   }
 
 
 
 
 
   render() {
     
       let  editform=null;
 
       let details;
      
  
                          
       if(this.state.edit)
       {
           details=null;
           editform =
          
           (  
               <div>
 
 
 
               <form onSubmit={this.submitPost}>
                   <div class="container">
                       <div class="row">
                           <div class="col-lg-10 col-xl-9 mx-auto">
                               <div class="card card-signin flex-row my-5">
                                   <div class="form-label-group">
                                       <label class="control-label col-sm-2" for="collegename">College Name:</label>
                                       <div class="col-sm-3">
 
                                           <input type="text" name="collegename" id="collegename" value={this.state.collegename}  onChange={this.onChange} class="form-control" required />
 
                                       </div><br /><br />
                                       <div class="form-label-group">
                                                   <label class="control-label col-sm-2" for="educationlocation">Location:</label>
                                                   <div class="col-sm-3">
                                                       <input type="text" name="educationlocation" id="educationlocation" value={this.state.educationlocation}  onChange={this.onChange} class="form-control" required />
                                                   </div>
                                               </div><br /><br />
                                       <div class="form-label-group">
                                           <label class="control-label col-sm-2" for="educationdegree">Degree:</label>
                                           <div class="col-sm-3">
 
 
                                               <input type="text" name="educationdegree" id="educationdegree" value={this.state.educationdegree} onChange={this.onChange} class="form-control" required />
                                           </div>
                                       </div><br /><br />
                                       <div class="form-label-group">
                                           <label class="control-label col-sm-2" for="educationmajor">Major:</label>
                                           <div class="col-sm-3">
                                               <input type="text" name="educationmajor" id="educationmajor" value={this.state.educationmajor} onChange={this.onChange} class="form-control" required />
                                           </div>
                                       </div><br /><br />
                                       <div class="form-label-group">
                                           <label class="control-label col-sm-2" for="educationyearofpassing">Year of passing</label>
                                           <div class="col-sm-3">
                                               <input type="year" name="educationyearofpassing" pattern="[0-9]+" value={this.state.educationyearofpassing}  id="educationyearofpassing" onChange={this.onChange} class="form-control" required />
                                           </div>
                                       </div><br /><br />
                                       <div class="form-label-group">
                                           <label class="control-label col-sm-2" for="educationcurrentcgpa">Current CGPA:</label>
                                           <div class="col-sm-3">
                                               <input type="text" name="educationcurrentcgpa" id="educationcurrentcgpa" value={this.state.educationcurrentcgpa} onChange={this.onChange} class="form-control" required />
                                           </div><br /><br />
                                       </div>
                                       <br /><br />
                                       <div class="form-group">
                                        
                                           <button onClick={this.submitEdit} class="btn btn-success" type="submit">Submit</button>&nbsp;
                                           <button onClick={this.handleedit} class="btn btn-success" type="submit">Cancel</button>&nbsp;
                                           <button onClick={this.submitDelete} class="btn btn-danger" type="submit">Delete</button>&nbsp;
       </div>
 
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </form>
           </div>
 
            
           );  
       }
       else{
           details= (
              
                   <div>
                   <div class="row">
                   <div class="col-lg-1"><span class="glyphicon glyphicon-education"  style={{fontSize:30}}></span></div>
                   <div class="col-lg-10">
                     
                         
                           <h4>College Name:{this.props.educationrecord.collegename}</h4>
                           <h4>Location:{this.props.educationrecord.educationlocation}</h4>
                           <h4>Degree:{this.props.educationrecord.educationdegree}</h4>
                           <h4>Major:{this.props.educationrecord.educationmajor}</h4>
                           <h4>Year of passing:{this.props.educationrecord.educationyearofpassing}</h4>
                           <h4>Current CGPA:{this.props.educationrecord.educationcurrentcgpa}</h4>
                           <div>{editform}</div>
                           </div>
                           <div class="col-lg-1">
                           <IconButton style={{fontSize:30}} onClick={()=>this.handleedit()}><Edit/></IconButton>
                             </div>
                           </div>
                      </div>
              
              
          
          
          
           )
       }
     
             return (
       <div>
              
              
              {details}
              {editform}
            
             
           
            
            
            
              
             
              </div>
             
              
           
             
             
           );
       }
}
 
 
 
 
Education2.propTypes = {
   getStudentprofile: PropTypes.func.isRequired,
   updateEducation: PropTypes.func.isRequired,
   deleteEducation :PropTypes.func.isRequired,
 
};
 
 
export default connect(null, { getStudentprofile, updateEducation, deleteEducation })(Education2);
 
