/* eslint-disable no-undef */
import React, { Component } from 'react';
//import '../../App.css';

import IconButton from '@material-ui/core/IconButton';
import { Edit } from '@material-ui/icons';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getStudentprofile,updateStudentcareerobjective } from '../../../actions/profileActions';


export class Careerobjective extends Component {
    constructor() {
        super();
        this.state = {

            authFlag: 0,

            edit: false,
           


        }


        this.onChange = this.onChange.bind(this);
        this.handleedit = this.handleedit.bind(this);
        this.changeHandler = this.changeHandler.bind(this);

    }
    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    // componentDidMount() {
    //     const data ={
    //         studentid:sessionStorage.getItem('studentid')

    //     };
    //     this.props.getStudentprofile(data);

        
    // }
   componentWillReceiveProps(nextProps) {
        if (nextProps.profile) {
            var { profile } = nextProps;

            var userData = {
                add: false,
                edit: false,
               
                careerobjective: profile.careerobjective || this.state.careerobjective,
               
            };

            this.setState(userData);
        }
    }
    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    
        //get the books data from backend   componentDidMount() {

        submitEdit = (e) => {

          
            e.preventDefault();

            const data = {
                studentid: sessionStorage.getItem('studentid'),
                careerobjective: this.state.careerobjective
            }

            this.props.updateStudentcareerobjective(data);
            this.handleedit();

        }


        handleedit = () => {
            this.setState({
                edit: !this.state.edit

            })
        }
        changeHandler = (e) => {
            this.setState({
                [e.target.name]: e.target.value
            })
        }




        render()
        {


            let editform = null;

            let details;

            if (this.state.edit) {
                details = null;

                editform =

                    (
                        <div>



                            <form onSubmit={this.submitPost}>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-10 col-xl-9 mx-auto">
                                            <div class="card card-signin flex-row my-5">
                                                <div class="form-label-group">
                                                    <div class="col-sm-7">
                                                        <div class="style__text___2ilXR style__info___18ZP5">
                                                            What are you passionate about? What are you looking for on Handshake? What are your experiences or skills?</div>
                                                        <textarea name="careerobjective" id="careerobjective" value={this.state.careerobjective}  onChange={this.onChange} placeholder="Type your introduction..." rows="4" type="textarea"
                                                             class="form-control" required></textarea>

                                                    </div><br /><br />



                                                    <div class="form-group">
                                                        <button onClick={this.submitEdit} class="btn btn-success" type="submit">Save</button>&nbsp;
                                                    <button onClick={this.handleedit} class="btn btn-success" type="submit">Cancel</button>&nbsp;
        </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>


                    );
            }
            else {
                if (this.props.profile.length>0) {
                    details = 
                         (
                            <div>
                                <div class="row">
                                    <div class="col-lg-1"></div>
                                    <div class="col-lg-11">



                                        <h4>{this.props.profile[0].careerobjective}</h4>
                                        <div>{editform}</div>
                                    </div>

                                </div>
                            </div>
                        )
                         }
                         else {
                            console.log("2");
                            details =
                                (
                                    <div>
                                    <div class="row">
                                        <div class="col-lg-1"></div>
                                        <div class="col-lg-11">
    
    
    
                                        <h4>{this.state.careerobjective}</h4>
                                            <div>{editform}</div>
                                        </div>
    
                                    </div>
                                </div>
                                   
                                )
            
                        }         
           }
                return (
                    <div>

                        <div class="row">
                            <div class="col-lg-11"> <h3>Career objective</h3></div>

                            <div class="col-lg-1"> <IconButton style={{ fontSize: 30 }} onClick={() => this.handleedit()}><Edit /></IconButton></div>
                        </div>


                        <div class="row">




                            {details}
                            {editform}
                        </div>



                    </div>
                );
            }
        }
    
    
        
   Careerobjective.propTypes = {
    getStudentprofile: PropTypes.func.isRequired,
    updateStudentprofile: PropTypes.func.isRequired,
    profile: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
            profile: state.profile.profile,
 });
 export default connect(mapStateToProps, { getStudentprofile, updateStudentcareerobjective})(Careerobjective);
