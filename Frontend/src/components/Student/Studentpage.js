import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { getAllotherstudents } from '../../actions/studentActions';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import TablePagination from '@material-ui/core/TablePagination';







class Studentpage extends Component {
    
    constructor() {
        super();
        this.state = {
            collegenameSearch: "",
            studentnameSearch: "",
            majorSearch: "",
            filterlist: [],
            rowsPerPage: 5,
            studentlist: [],
            page: 0,
            passvar:false



        }
        this.collegenameSearchHandler = this.collegenameSearchHandler.bind(this);
        this.studentnameSearchHandler = this.studentnameSearchHandler.bind(this);
        this.majorSearchHandler = this.majorSearchHandler.bind(this);
        this.applyfilter = this.applyfilter.bind(this);
       // this.handleredirect=this.handleredirect.bind(this);



    }
  
    componentWillReceiveProps(nextProps) {
        if (nextProps.studentlist) {
            var { studentlist } = nextProps;

            this.setState({
                filterlist: studentlist

            });
        }
    }
    componentDidMount() {
        const data = {
            studentid: sessionStorage.getItem('studentid')
        }

        this.props.getAllotherstudents(data);

    }

    studentnameSearchHandler(e) {
        this.applyfilter(e.target.value.toLowerCase(), this.state.collegenameSearch.toLowerCase(), this.state.majorSearch.toLowerCase())

        this.setState({
            studentnameSearch: e.target.value
        });

    }
    collegenameSearchHandler(e) {
        this.applyfilter(this.state.studentnameSearch.toLowerCase(), e.target.value.toLowerCase(), this.state.majorSearch.toLowerCase())

        this.setState({
            collegenameSearch: e.target.value
        });

    }
    majorSearchHandler(e) {
        this.applyfilter(this.state.studentnameSearch.toLowerCase(), this.state.collegenameSearch.toLowerCase(), e.target.value.toLowerCase())

        this.setState({
            majorSearch: e.target.value
        });

    }
    applyfilter = (p1, p2, p3) => {
        if (p1 === "" && p2 === "" && p3 === "") {
            this.setState({
                filterlist: this.props.studentlist

            })

        }
        else {
            var resultlist = [];

            for (var i = 0; i < this.props.studentlist.length; i++) {
                if (this.props.studentlist[i] != null && this.props.studentlist.length > 0) {
                    if ((this.props.studentlist[i].name.toLowerCase().includes(p1)) &&
                        (this.props.studentlist[i].collegename.toLowerCase().includes(p2))
                        && (this.props.studentlist[i].education[0] ? this.props.studentlist[i].education[0].educationmajor.toLowerCase().includes(p3) : "")) {
                        resultlist.push(this.props.studentlist[i])
                    }



                }

            }
            this.setState({
                filterlist: resultlist

            })
        }

    }
    handleChangePage = (event, newPage) => {
        this.setState({
            page: newPage
        })
    };
    handleChangeRowsPerPage = (event) => {
        this.setState({
            rowsPerPage: (parseInt(event.target.value, 10)),
            page: 0,
        })


    };
    render() {
    
        let details = (
            this.state.rowsPerPage > 0
            ? this.state.filterlist.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage)
            : this.state.filterlist
        ).map((studentlist, index) => {
            return(
               
            



            <div class="well" text-right>
               <div class="form-group row">
                    <div class="col-md-8">
            
                          <h5> Name:   {studentlist.name}</h5>
                          <h5> Mail ID:  {studentlist.mail}</h5>
                           <h5>Collegename :  {studentlist.collegename}</h5>
                           <h5>Major :  {studentlist.education[0] ? studentlist.education[0].educationmajor : ""}</h5>



                            <h5>Students profile:<Link to={"/Studentprofileout/" + studentlist.studentid}>click here</Link> </h5>
                    </div>
                    <div class="col-md-3"><br></br><br></br><br></br> 
                    <Link to={{ pathname: "/Messages",
                                       state: {
                                             studentlist:studentlist
                                            } 
                        }}   className="btn btn-primary">Message</Link>
                               
                     
                    </div>
                </div>
                </div>

            )
    })   

        //if not logged in go to login page

return (
    <div class="paddingleft15">


        <div class="form-group row">
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <div class="well" text-right>
                    <h2>Filters</h2><br></br>
                    <h3>Student Name</h3><br></br>
                    <input type="text" onChange={this.studentnameSearchHandler} class="form-control" name="studentsearch"
                        placeholder=" Search by student name" required /><br></br>
                    <h3>College Name</h3><br></br>
                    <input type="text" onChange={this.collegenameSearchHandler} class="form-control" name="studentsearch"
                        placeholder=" Search by college name" required /><br></br>
                    <h3>Major</h3><br></br>
                    <input type="text" onChange={this.majorSearchHandler} class="form-control" name="studentsearch"
                        placeholder=" Filter by major" required /><br></br>




                </div>
            </div>
            <div class="col-md-6">
                {details}
                <h1><TablePagination
                    style={{ fontSize: 10 }}
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={this.state.filterlist.length}
                    rowsPerPage={this.state.rowsPerPage}
                    page={this.state.page}
                    onChangePage={this.handleChangePage}
                    onChangeRowsPerPage={this.handleChangeRowsPerPage} /></h1>

            </div>

        </div>



    </div>

)

    }
}
//export Home Component
Studentpage.propTypes = {

    getAllstudents: PropTypes.func.isRequired,

};

const mapStateToProps = state => ({
    studentlist: state.studentlist.studentlist,
});

export default connect(mapStateToProps, { getAllotherstudents })(Studentpage);
