# handshakeproject
Technologies: MongoDB, Express, React, NodeJs, Passport-JWT, Kafka, Redux, AWS EC2


Steps to use this project:

Git Clone Repository
Change Directory in terminal to /Backend and use command 'npm install' to install all dependencies.
Change Directory in terminal to /Frontend and use command 'npm install' to install all dependencies.
Create a MongoDB and configure the same in Backend/src/helper file
Install Zookeeper, Kafka on your system and run
Open Terminal in /Backend and run "npm run dev"
Open Terminal in /Kafka-Backend and run "npm run dev"
Open another Terminal in /Frontend and run "npm start"
Use the application in Browser
